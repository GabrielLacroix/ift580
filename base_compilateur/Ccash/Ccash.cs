﻿using System.IO;
using Antlr4.Runtime;
using Ccash.Antlr;
using Ccash.CodeGeneration.LLVMGenerator;
using Ccash.SemanticAnalysis;

namespace Ccash
{
    public static class Ccash
    {
        public static void Main(string[] args)
        {
            var inputFilePath = "test_files/main.ccash";
            var file = new AntlrFileStream(inputFilePath);
            var lexer = new CcashLexer(file);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new CcashParser(tokenStream);
            
            var tree = new AbstractSyntaxTree(parser.compilationUnit());

            if (ErrorsManager.HasErrors)
            {
                foreach(string error in ErrorsManager.Errors)
                {
                    System.Console.WriteLine(error);
                }
            }
            else
            {
                var generator = new LLVMCodeGenerator();
                generator.Generate(tree, Path.GetFileNameWithoutExtension(inputFilePath));
            }

            System.Console.WriteLine("Appuyez sur une touche pour continuer...");
            System.Console.Read();
        }
    }
}