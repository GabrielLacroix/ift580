using System.Linq;
using Ccash.Types;

namespace Ccash
{
    public static class FunctionNameMangler
    {
        public static string Mangle(string identifier, params CcashType[] parameterTypes)
        {
            if (identifier == "main" || identifier == "printf")
            {
                return identifier;
            }
            
            return $"{identifier}::<{string.Join(",", parameterTypes.Select(t => t.Name))}>";
        }
    }
}