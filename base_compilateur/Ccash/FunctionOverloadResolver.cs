using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.SemanticAnalysis;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash
{
    public static class FunctionOverloadResolver
    {
        public static (string, CcashType, List<Expression>) Resolve(string identifier, IEnumerable<Expression> arguments, SymbolTable symbolTable)
        {
            arguments = arguments.ToList();
            var overloads = symbolTable.GetFunctionOverloads(identifier);
            var resolvedFunctionType = Resolve(overloads, arguments.Select(a => a.Type));

            arguments = arguments.Zip(resolvedFunctionType.ParameterTypes, (argumentType, parameterType) =>
            {
                if (argumentType.Type == parameterType)
                {
                    return argumentType;
                }

                var constructor = FunctionNameMangler.Mangle(parameterType.Name, argumentType.Type);
                return new FunctionCallExpression(constructor, symbolTable, argumentType);
            });

            var functionName = FunctionNameMangler.Mangle(identifier, arguments.Select(a => a.Type).ToArray());
            return (functionName, resolvedFunctionType.ReturnType, arguments.ToList());
        }

        private static FunctionType Resolve(IEnumerable<FunctionType> overloads, IEnumerable<CcashType> argumentTypes)
        {
            argumentTypes = argumentTypes.ToList();
            overloads = FilterOverloads(overloads, argumentTypes).ToList();

            if (!overloads.Any())
            {
                throw new NotImplementedException();
            }

            var min = int.MaxValue;
            var minOverload = overloads.FirstOrDefault();

            foreach (var overload in overloads)
            {
                var promotionDistance = overload.ParameterTypes.Zip(argumentTypes,
                    (parameterType, argumentType) => argumentType.PromotionDistance(parameterType)).Sum();
                
                if (promotionDistance < min)
                {
                    min = promotionDistance;
                    minOverload = overload;
                }
            }

            return minOverload;
        }

        private static IEnumerable<FunctionType> FilterOverloads(IEnumerable<FunctionType> overloads, IEnumerable<CcashType> argumentTypes)
        {
            return overloads
                .Where(overload => overload.ParameterTypes.Count == argumentTypes.Count())
                .Where(overload => overload.ParameterTypes.Zip(argumentTypes,
                    (parameterType, argumentType) => argumentType.IsPromotable(parameterType)).All(o => o));
        }
    }
}