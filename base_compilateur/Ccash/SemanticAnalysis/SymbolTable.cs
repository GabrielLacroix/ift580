using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Ccash.SemanticAnalysis.Nodes;
using Ccash.Types;

namespace Ccash.SemanticAnalysis
{
    public class SymbolTable
    {
        private Dictionary<string, SymbolInfo> Symbols { get; } = new Dictionary<string, SymbolInfo>();

        private SymbolTable Parent { get; }

        private IScope Owner { get; }

        public SymbolTable(IScope owner)
        {
            Owner = owner;
        }

        public SymbolTable(SymbolTable parent, IScope owner)
        {
            Parent = parent;
            Owner = owner;
        }
        
        public void AddSymbol(string symbol, CcashType type, bool isMutable)
        {
            if (!Symbols.TryAdd(symbol, new SymbolInfo(type, isMutable)))
            {
                throw new NotImplementedException();
            }
        }

        public void AddImmutableSymbol(string symbol, CcashType type)
        {
            AddSymbol(symbol, type, false);
        }
        
        public virtual bool Contains(string symbol)
        {
            return Symbols.ContainsKey(symbol) || (Parent?.Contains(symbol) ?? false);
        }

        public virtual bool IsSymbolAFunctionParameter(string symbol)
        {
            return Parent != null && Parent.IsSymbolAFunctionParameter(symbol);
        }

        public T FindEnclosing<T>()
        {
            if (Owner is T owner)
            {
                return owner;
            }

            if (Parent == null)
            {
                throw new NotImplementedException();
            }

            return Parent.FindEnclosing<T>();
        }

        public IEnumerable<FunctionType> GetFunctionOverloads(string identifier)
        {
            return GetMatchingSymbols($"^(.+::)?{Regex.Escape(identifier)}::<.*>")
                .Where(s => s.Type is FunctionType)
                .Select(s => (FunctionType) s.Type);
        }

        public T GetCodeGeneratorData<T>(string symbol)
        {
            var codeGeneratorData = GetSymbolInfo(symbol).CodeGeneratorData;
            if (codeGeneratorData == null)
            {
                throw new NullReferenceException();
            }

            return (T) codeGeneratorData;
        }

        public SymbolInfo this[string symbol] => GetSymbolInfo(symbol);

        protected virtual SymbolInfo GetSymbolInfo(string symbol)
        {
            if (!Contains(symbol))
            {
                throw new NotImplementedException();
            }

            if (Symbols.TryGetValue(symbol, out var info))
            {
                return info;
            }

            if (Parent == null)
            {
                throw new NotImplementedException();
            }

            return Parent.GetSymbolInfo(symbol);
        }

        private IEnumerable<SymbolInfo> GetMatchingSymbols(string pattern)
        {
            var symbols = new List<SymbolInfo>();
            for (SymbolTable curr = this; curr != null; curr = curr.Parent)
            {
                symbols.AddRange(curr.Symbols.Where(p => Regex.IsMatch(p.Key, pattern)).Select(p => p.Value));
            }

            return symbols;
        }
    }
}