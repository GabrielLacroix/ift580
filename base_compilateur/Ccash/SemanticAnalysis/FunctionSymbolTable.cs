using System;
using System.Collections.Generic;
using Ccash.SemanticAnalysis.Nodes;

namespace Ccash.SemanticAnalysis
{
    public class FunctionSymbolTable : SymbolTable
    {
        private Dictionary<string, SymbolInfo> Parameters { get; } = new Dictionary<string, SymbolInfo>();

        public FunctionSymbolTable(SymbolTable parent, FunctionDeclaration functionDeclaration) : base(parent, functionDeclaration)
        {
        }
        
        public void AddParameter(Parameter parameter)
        {
            if (!Parameters.TryAdd(parameter.Name, new SymbolInfo(parameter.Type, parameter.IsMutable)))
            {
                throw new NotImplementedException();
            }
        }

        public override bool Contains(string symbol)
        {
            return Parameters.ContainsKey(symbol) || base.Contains(symbol);
        }

        public override bool IsSymbolAFunctionParameter(string symbol)
        {
            return Parameters.ContainsKey(symbol);
        }

        protected override SymbolInfo GetSymbolInfo(string symbol)
        {
            return Parameters.ContainsKey(symbol) ? Parameters[symbol] : base.GetSymbolInfo(symbol);
        }
    }
}