using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes;

namespace Ccash.SemanticAnalysis
{
    public class AbstractSyntaxTree
    {
        public CompilationUnit CompilationUnit { get; }

        public AbstractSyntaxTree(CcashParser.CompilationUnitContext context)
        {
            CompilationUnit = new CompilationUnit(context);
        }
    }
}