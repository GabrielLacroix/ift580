namespace Ccash.SemanticAnalysis.Nodes
{
    public interface IScope
    {
        SymbolTable SymbolTable { get; }
    }
}