using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Statements;

namespace Ccash.SemanticAnalysis.Nodes
{
    public class Block
    {
        public List<Statement> Statements { get; }

        public Block(CcashParser.BlockContext context, SymbolTable symbolTable)
        {
            Statements = context.statement()?.Select(s => Statement.Create(s, symbolTable)).ToList();
            if (Statements == null)
            {
                Statements = new List<Statement>();
            }
        }

    }
}