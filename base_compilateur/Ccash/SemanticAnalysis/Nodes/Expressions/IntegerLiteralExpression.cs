using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class IntegerLiteralExpression : Expression
    {
        public override CcashType Type => CcashType.Int32;
        
        public override string Text => $"{Value}";

        public int Value { get; }

        public IntegerLiteralExpression(CcashParser.IntegerLiteralExpressionContext context)
        {
            Value = int.Parse(context.IntegerLiteral().GetText());
        }
    }
}