﻿using System;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class ReferenceExpression : Expression
    {
        public override CcashType Type { get; }
        
        public ReferenceType TypeAsReference => (ReferenceType) Type;
        
        public CcashType ReferredType => TypeAsReference.Type;
        
        public override string Text => $"&{Identifier}";
        
        public string Identifier { get; }

        public ReferenceExpression(string identifier, SymbolTable symbolTable)
        {
            if (!symbolTable.Contains(identifier))
            {
                throw new NotImplementedException();
            }

            var symbolInfo = symbolTable[identifier];
            Type = new ReferenceType(symbolInfo.Type, symbolInfo.IsMutable);
            Identifier = identifier;
        }
    }
}
