using System;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class IdentifierExpression : Expression
    {
        public override CcashType Type { get; }
        
        public override string Text => Identifier;

        public string Identifier { get; }

        public IdentifierExpression(CcashParser.IdentifierExpressionContext context, SymbolTable symbolTable)
        {
            var identifier = context.Identifier().GetText();
            if (!symbolTable.Contains(identifier))
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'identifiant '{2}' n'est pas d�fini.", context?.Start.Line, context?.Start.Column, identifier));
                return;
            }
            
            Type = symbolTable[identifier].Type;
            Identifier = identifier;
        }
        
        public IdentifierExpression(string identifier, SymbolTable symbolTable)
        {
            if (!symbolTable.Contains(identifier))
            {
                throw new NotImplementedException();
            }
            
            Type = symbolTable[identifier].Type;
            Identifier = identifier;
        }
    }
}