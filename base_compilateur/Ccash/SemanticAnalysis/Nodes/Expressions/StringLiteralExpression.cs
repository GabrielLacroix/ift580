using System.Text.RegularExpressions;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class StringLiteralExpression : Expression
    {
        public override CcashType Type => CcashType.String;
        
        public override string Text => Value;

        public string Value { get; }

        public StringLiteralExpression(CcashParser.StringLiteralExpressionContext context)
        {
            var val = context.StringLiteral().GetText();
            Value = Regex.Unescape(val.Substring(1, val.Length - 2));
        }
    }
}