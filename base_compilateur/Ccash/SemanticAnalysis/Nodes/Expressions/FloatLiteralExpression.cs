﻿using System.Globalization;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class FloatLiteralExpression : Expression
    {
        public override CcashType Type { get; }

        public override string Text => $"{Value}";

        public double Value { get; }

        public FloatLiteralExpression(CcashParser.FloatLiteralExpressionContext context)
        {
            Type = CcashType.Float64;
            Value = double.Parse(context.FloatLiteral().GetText(), CultureInfo.InvariantCulture);
        }

        public FloatLiteralExpression(CcashParser.Float32LiteralExpressionContext context)
        {
            Type = CcashType.Float32;
            var text = context.Float32Literal().GetText();
            text = text.Substring(0, text.Length - 1);
            Value = float.Parse(text, CultureInfo.InvariantCulture);
        }
    }
}