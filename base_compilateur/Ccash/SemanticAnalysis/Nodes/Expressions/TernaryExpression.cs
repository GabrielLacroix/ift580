using System;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class TernaryExpression : Expression
    {
        public override CcashType Type { get; }

        public override string Text { get; }
        
        public Expression ConditionExpression { get; }
        
        public Expression TrueExpression { get; }
        
        public Expression FalseExpression { get; }

        public TernaryExpression(CcashParser.TernaryExpressionContext context, SymbolTable symbolTable)
        {
            Text = context.GetText();

            ConditionExpression = Create(context.expression(0), symbolTable);
            TrueExpression = Create(context.expression(1), symbolTable);
            FalseExpression = Create(context.expression(2), symbolTable);

            if (ConditionExpression.Type != CcashType.Boolean)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit �tre de type Bool�en.", context.expression(0).Start.Line, context.expression(0).Start.Column));
            }

            if (TrueExpression.Type != FalseExpression.Type)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : Le type des op�randes doit �tre le m�me.", context.expression(1).Start.Line, context.expression(1).Start.Column));
            }

            Type = TrueExpression.Type;
        }
    }
}