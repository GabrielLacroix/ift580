using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class FunctionCallExpression : Expression
    {
        public override CcashType Type { get; }
        
        public override string Text => $"{FunctionName}({string.Join(',', Arguments.Select(a => a.Text))})";

        public string FunctionName { get; }
        
        public List<Expression> Arguments { get; }
        
        public FunctionCallExpression(CcashParser.FunctionCallExpressionContext context, SymbolTable symbolTable)
        {
            var arguments = context
                            .functionCall()
                            .functionArgs()
                            ?.expression()
                            ?.Select(e => Create(e, symbolTable))
                            .ToList() ?? new List<Expression>();

            var identifier = context.functionCall().functionName().GetText();
            (FunctionName, Type, Arguments) = FunctionOverloadResolver.Resolve(identifier, arguments, symbolTable);
            ValidateTypes(context, symbolTable);
        }

        public FunctionCallExpression(string functionName, SymbolTable symbolTable, params Expression[] arguments)
        {
            FunctionName = functionName;
            Arguments = arguments.ToList();
            Type = (symbolTable[FunctionName].Type as FunctionType)?.ReturnType ?? CcashType.Void;
            ValidateTypes(null, symbolTable);
        }

        public FunctionCallExpression(CcashType ownerType, string methodName, SymbolTable symbolTable, params Expression[] arguments)
        {
            var identifier = $"{ownerType.Name}::{methodName}";
            (FunctionName, Type, Arguments) = FunctionOverloadResolver.Resolve(identifier, arguments, symbolTable);
            ValidateTypes(null, symbolTable);
        }

        private void ValidateTypes(CcashParser.FunctionCallExpressionContext context, SymbolTable symbolTable)
        {
            if (FunctionName == "printf")
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : La fonction 'printf' ne retourne aucune valeur.", context?.functionCall().Start.Line, context?.functionCall().Start.Column));
            }
            
            if (symbolTable[FunctionName].Type is FunctionType functionType)
            {
                if (functionType.ReturnType == CcashType.Void)
                {
                    ErrorsManager.AddError(String.Format("{0}:{1} : La fonction '{2}' ne retourne aucune valeur.", context?.functionCall().Start.Line, context?.functionCall().Start.Column, FunctionName));
                }

                if (!functionType.ParameterTypes.SequenceEqual(Arguments.Select(s => s.Type)))
                {
                    ErrorsManager.AddError(String.Format("{0}:{1} : Le type des param�tres de la fonction doit �tre le m�me que le type des arguments.", context?.functionCall().Start.Line, context?.functionCall().Start.Column));
                }
            }
            else
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : '{2}' n'est pas une fonction.", context?.functionCall().Start.Line, context?.functionCall().Start.Column, FunctionName));
            }
        }
    }
}