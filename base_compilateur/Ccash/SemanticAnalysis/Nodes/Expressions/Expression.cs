using System;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public abstract class Expression
    {
        public abstract CcashType Type { get; }

        public abstract string Text { get; }
        
        public static Expression Create(CcashParser.ExpressionContext context, SymbolTable symbolTable)
        {
            switch (context)
            {
                case CcashParser.IntegerLiteralExpressionContext integerLiteralContext:
                    return new IntegerLiteralExpression(integerLiteralContext);

                case CcashParser.Float32LiteralExpressionContext float32LiteralExpressionContext:
                    return new FloatLiteralExpression(float32LiteralExpressionContext);

                case CcashParser.FloatLiteralExpressionContext floatLiteralContext:
                    return new FloatLiteralExpression(floatLiteralContext);

                case CcashParser.StringLiteralExpressionContext stringLiteralContext:
                    return new StringLiteralExpression(stringLiteralContext);

                case CcashParser.BooleanLiteralExpressionContext booleanLiteralContext:
                    return new BooleanLiteralExpression(booleanLiteralContext);

                case CcashParser.IdentifierExpressionContext identifierContext:
                    return new IdentifierExpression(identifierContext, symbolTable);

                case CcashParser.FunctionCallExpressionContext functionCallContext:
                    return new FunctionCallExpression(functionCallContext, symbolTable);

                case CcashParser.BinaryOperatorExpressionContext binaryOperatorContext:
                    return BinaryOperatorToMethodCall(binaryOperatorContext, symbolTable);

                case CcashParser.UnaryOperatorExpressionContext unaryOperatorContext:
                    return UnaryOperatorToMethodCall(unaryOperatorContext, symbolTable);

                case CcashParser.ParenthesisExpressionContext parenthesisExpressionContext:
                    return Create(parenthesisExpressionContext.expression(), symbolTable);
                
                case CcashParser.TernaryExpressionContext ternaryExpressionContext:
                    return new TernaryExpression(ternaryExpressionContext, symbolTable);

                default:
                    throw new NotImplementedException();
            }
        }

        private static Expression BinaryOperatorToMethodCall(CcashParser.BinaryOperatorExpressionContext context, SymbolTable symbolTable)
        {
            var rightOperand = Create(context.expression()[0], symbolTable);
            var leftOperand = Create(context.expression()[1], symbolTable);
            var op = context.children[1].GetText();

            return new FunctionCallExpression(rightOperand.Type, $"operator{op}", symbolTable, rightOperand, leftOperand);
        }

        private static Expression UnaryOperatorToMethodCall(CcashParser.UnaryOperatorExpressionContext context, SymbolTable symbolTable)
        {
            var operand = Create(context.expression(), symbolTable);
            var op = context.children[0].GetText();

            return new FunctionCallExpression(operand.Type, $"operator{op}", symbolTable, operand);
        }
    }
}