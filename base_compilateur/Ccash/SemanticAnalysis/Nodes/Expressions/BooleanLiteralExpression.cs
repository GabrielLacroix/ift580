using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Expressions
{
    public class BooleanLiteralExpression : Expression
    {
        public override CcashType Type => CcashType.Boolean;
        
        public override string Text => $"{Value}";
        
        public bool Value { get; }
        
        public BooleanLiteralExpression(CcashParser.BooleanLiteralExpressionContext context)
        {
            Value = bool.Parse(context.GetText());
        }
    }
}