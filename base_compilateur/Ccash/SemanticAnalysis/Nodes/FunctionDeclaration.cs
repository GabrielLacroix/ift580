using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes
{
    public struct Parameter
    {
        public string Name { get; }
        public CcashType Type { get; }
        public bool IsMutable { get; }

        public Parameter(CcashParser.VariableContext context)
        {
            Name = context.Identifier().GetText();
            Type = CcashType.Create(context.variableType());
            IsMutable = context.Mut() != null;
        }
    }

    public class FunctionDeclaration : IScope
    {
        public SymbolTable SymbolTable { get; }

        public string Name { get; }

        public List<Parameter> Parameters { get; }

        public FunctionType FunctionType { get; }

        public Block Block { get; }

        public FunctionDeclaration(CcashParser.FunctionDeclarationContext context, SymbolTable parentSymbolTable)
        {
            Parameters = context.functionParameters()?.variable()?.Select(v => new Parameter(v)).ToList();
            if (Parameters == null)
            {
                Parameters = new List<Parameter>();
            }

            var identifier = context.Identifier().GetText();
            Name = FunctionNameMangler.Mangle(identifier, Parameters.Select(p => p.Type).ToArray());

            FunctionType = new FunctionType(context);

            SymbolTable = new FunctionSymbolTable(parentSymbolTable, this);

            var functionSymbolTable = (FunctionSymbolTable) SymbolTable;
            Parameters.ForEach(functionSymbolTable.AddParameter);

            Block = new Block(context.block(), SymbolTable);
        }
    }
}