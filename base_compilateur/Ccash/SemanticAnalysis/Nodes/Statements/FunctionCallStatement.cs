using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public class FunctionCallStatement : Statement
    {
        public string FunctionName { get; }

        public List<Expression> Arguments { get; }

        public FunctionCallStatement(CcashType ownerType, string methodName, SymbolTable symbolTable, params Expression[] arguments)
        {
            var identifier = $"{ownerType.Name}::{methodName}";
            (FunctionName, _, Arguments) = FunctionOverloadResolver.Resolve(identifier, arguments, symbolTable);
            ValidateTypes(null, symbolTable);
        }

        public FunctionCallStatement(CcashParser.FunctionCallContext context, SymbolTable symbolTable)
        {
            var arguments = context
                                .functionArgs()
                                ?.expression()
                                ?.Select(e => Expression.Create(e, symbolTable))
                                .ToList() ?? new List<Expression>();

            var identifier = context.functionName().GetText();
            if (identifier == "printf")
            {
                FunctionName = identifier;
                Arguments = arguments;
            }
            else
            {
                (FunctionName, _, Arguments) = FunctionOverloadResolver.Resolve(identifier, arguments, symbolTable);
            }
            
            ValidateTypes(context, symbolTable);
        }

        private void ValidateTypes(CcashParser.FunctionCallContext context, SymbolTable symbolTable)
        {
            if (FunctionName == "printf")

            {
                if (Arguments.FirstOrDefault()?.Type != CcashType.String)
                {
                    ErrorsManager.AddError(String.Format("{0}:{1} : La fonction 'printf' requiert au moins un param�tre 'string'.", context?.functionName().Start.Line, context?.functionName().Start.Column));
                }

                return;
            }

            if (symbolTable[FunctionName].Type is FunctionType functionType)
            {
                // TODO : Is this really a constraint? 
                //Not be able to call a function with a return type without using the result shouldn't be an error...
                if (functionType.ReturnType != CcashType.Void)
                {
                    throw new NotImplementedException();
                }

                if (!functionType.ParameterTypes.SequenceEqual(Arguments.Select(s => s.Type)))
                {
                    ErrorsManager.AddError(String.Format("{0}:{1} : Le type des param�tres de la fonction doit �tre le m�me que le type des arguments.", context?.functionName().Start.Line, context?.functionName().Start.Column));
                }
            }
            else
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : '{2}' n'est pas une fonction.", context?.functionName().Start.Line, context?.functionName().Start.Column, FunctionName));
            }
        }
    }
}