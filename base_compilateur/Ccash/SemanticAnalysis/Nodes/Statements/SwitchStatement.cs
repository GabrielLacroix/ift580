﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using LLVMSharp;
using static Ccash.Antlr.CcashParser;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{

    class SwitchStatement : Statement, IScope, Breakable, Fallthrouable
    {
        public SymbolTable SymbolTable { get; }

        public Expression Expression { get; }

        public LLVMBasicBlockRef NextBlock { get; set; }

        public bool HasFallThrough { get; }

        public List<CaseStatement> CaseStatements { get; }

        public DefaultStatement DefaultStatement { get; }

        public SwitchStatement(CcashParser.SwitchStatementContext context, SymbolTable parentSymbolTable)
        {
            if (context.expression() == null)
                return;

            SymbolTable = new SymbolTable(parentSymbolTable, this);

            HasFallThrough = context.fallthrough() != null;

            CaseStatements = context.caseStatement()?.Select(case0 => new CaseStatement(case0, SymbolTable)).ToList() ??
                new List<CaseStatement>();

            Expression = Expression.Create(context.expression(), parentSymbolTable);


            var caseStatementContexts = context.caseStatement();
            ISet<ExpressionContext> caseStatementExpressions = new HashSet<ExpressionContext>();
            Array.ForEach(caseStatementContexts, caseStatement => caseStatementExpressions.Add(caseStatement.expression()));
            if (caseStatementContexts.Length != caseStatementExpressions.Count)
            {
                ErrorsManager.AddError(System.String.Format("{0}:{1} : Il ne peut pas y avoir deux case avec la meme valeur.", context.expression().Start.Line, context.expression().Start.Column));
            }

            if (context.defaultStatement() != null)
            {
                DefaultStatement = new DefaultStatement(context.defaultStatement(), SymbolTable);
            }

        }

        LLVMBasicBlockRef Breakable.NextBlock()
        {
            return NextBlock;
        }

        // Je sais pas si ca va servir
        public CaseStatement getNext(CaseStatement current)
        {
            int index = CaseStatements.IndexOf(current);

            //if (index == CaseStatements.Count - 1)
            //    return DefaultStatement;

            return CaseStatements[index + 1];
        }
    }
}
