﻿using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    class CaseBlock
    {
        public List<Statement> Statements { get; }

        public bool HasFallthrough { get; }

        public CaseBlock(CcashParser.CaseBlockContext context, SymbolTable parentSymbolTable)
        {
            Statements = context.statement()?.Select(s => Statement.Create(s, parentSymbolTable)).ToList();
            if (Statements == null)
            {
                Statements = new List<Statement>();
            }
        }


    }
}
