﻿using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.SemanticAnalysis.Nodes.Statements.Loops;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    class BreakStatement : Statement
    {

        public Breakable Parent { get; }

        public BreakStatement(CcashParser.BreakStatementContext context, SymbolTable symbolTable)
        {
            if (context.Parent == null)
                return;
            Parent = symbolTable.FindEnclosing<Breakable>();
            
            if (Parent == null)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : Le break doit se trouver dans une boucle.", context.Start.Line, context.Start.Column));
            }
        }
    }
}
