﻿using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.SemanticAnalysis.Nodes.Statements.Loops;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    class FallthroughStatement : Statement
    {

        public Fallthrouable Parent { get; }

        public FallthroughStatement(CcashParser.FallthroughContext context, SymbolTable symbolTable)
        {
            if (context.Parent == null)
                return;
            Parent = symbolTable.FindEnclosing<Fallthrouable>();

            if (Parent == null)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : Le fallthrough doit se trouver dans switch.", context.Start.Line, context.Start.Column));
            }
        }
    }
}
