using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public class ReturnStatement : Statement
    {
        public Expression Expression { get; }
        
        public ReturnStatement(CcashParser.ReturnStatementContext context, SymbolTable symbolTable)
        {
            if (context.expression() == null) 
                return;
            
            Expression = Expression.Create(context.expression(), symbolTable);

            var function = symbolTable.FindEnclosing<FunctionDeclaration>();

            if (Expression.Type != function.FunctionType.ReturnType)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : Le type de l'expression de retour doit �tre du m�me type que la fonction.", context.expression().Start.Line, context.expression().Start.Column));
            }
        }
    }
}