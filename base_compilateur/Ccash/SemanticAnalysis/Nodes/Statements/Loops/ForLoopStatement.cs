﻿using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements.Loops
{
    public class ForLoopStatement : LoopStatement
    {
        public Statement Declaration { get; }

        public Statement Assignment { get; }

        public Expression Expression { get; }

        public Block Block { get; }

        public ForLoopStatement(CcashParser.ForStatementContext context, SymbolTable parentSymbolTable) : base(parentSymbolTable)
        {
            if (context.forInitialization() != null)
            {
                Declaration = CreateStatement(context.forInitialization());
            }

            Expression = Expression.Create(context.expression(), SymbolTable);
            if (!(Expression.Type is BooleanType))
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit être de type Booléen.", context.expression().Start.Line, context.expression().Start.Column));
            }

            Block = new Block(context.block(), SymbolTable);

            if (context.forUpdate() != null)
            {
                Assignment = CreateStatement(context.forUpdate());
            }
        }

        private Statement CreateStatement(CcashParser.ForInitializationContext context)
        {
            if (context.variableDeclaration() != null)
            {
                return new VariableDeclaration(context.variableDeclaration(), SymbolTable);
            }

            if (context.reassignment() != null)
            {
                return AssignmentOperatorToMethodCall(context.reassignment(), SymbolTable);
            }

            if (context.functionCall() != null)
            {
                return new FunctionCallStatement(context.functionCall(), SymbolTable);
            }

            return new EmptyStatement();
        }

        private Statement CreateStatement(CcashParser.ForUpdateContext context)
        {
            if (context.reassignment() != null)
            {
                return AssignmentOperatorToMethodCall(context.reassignment(), SymbolTable);
            }

            if (context.functionCall() != null)
            {
                return new FunctionCallStatement(context.functionCall(), SymbolTable);
            }

            return new EmptyStatement();
        }
    }
}