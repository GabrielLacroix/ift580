using LLVMSharp;

namespace Ccash.SemanticAnalysis.Nodes.Statements.Loops
{
    public abstract class LoopStatement : Statement, IScope, Breakable
    {
        public SymbolTable SymbolTable { get; }

        public object ExpressionBlock { get; set; }

        public LLVMBasicBlockRef NextBlock { get; set; }

        protected LoopStatement(SymbolTable parentSymbolTable)
        {
            SymbolTable = new SymbolTable(parentSymbolTable, this);
        }

        LLVMBasicBlockRef Breakable.NextBlock()
        {
            return NextBlock;
        }
    }
}