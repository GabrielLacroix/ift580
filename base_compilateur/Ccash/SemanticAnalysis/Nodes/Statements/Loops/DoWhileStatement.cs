﻿using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements.Loops
{
    class DoWhileStatement : LoopStatement
    {
        public Expression Expression { get; }

        public Block Block { get; }

        public DoWhileStatement(CcashParser.DoStatementContext context, SymbolTable parentSymbolTable) : base(parentSymbolTable)
        {
            Expression = Expression.Create(context.expression(), SymbolTable);
            if (!(Expression.Type is BooleanType))
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit être de type Booléen.", context.expression().Start.Line, context.expression().Start.Column));
            }

            Block = new Block(context.block(), SymbolTable);
        }

    }
}
