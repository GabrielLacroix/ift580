﻿using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ccash.SemanticAnalysis.Nodes.Statements.Loops
{
    class RepeatStatement : LoopStatement
    {
        public Expression Expression { get; }

        public Block Block { get; }

        public RepeatStatement(CcashParser.RepeatStatementContext context, SymbolTable parentSymbolTable) : base(parentSymbolTable)
        {
            Expression = Expression.Create(context.expression(), SymbolTable);
            if (!(Expression.Type is IntegerType))
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit être un entier non signe.", context.expression().Start.Line, context.expression().Start.Column));
            }

            Block = new Block(context.block(), SymbolTable);
        }
    }
}
