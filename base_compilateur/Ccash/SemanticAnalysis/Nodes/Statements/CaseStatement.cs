﻿using System;
using System.Collections.Generic;
using System.Text;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    class CaseStatement : Statement, IScope, Fallthrouable
    {

        public SymbolTable SymbolTable { get; }

        public bool HasFallThrough { get; }

        public Expression Expression { get; }

        public CaseBlock CaseBlock { get; }



        public CaseStatement(CcashParser.CaseStatementContext context, SymbolTable parentSymbolTable)
        {
            SymbolTable = new SymbolTable(parentSymbolTable, this);

            CaseBlock = new CaseBlock(context.caseBlock(), SymbolTable);

            Expression = Expression.Create(context.expression(), parentSymbolTable);

            if (!Expression.Type.IsIntegral && !(Expression.Type is BooleanType))
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit être de type Integral.", context.expression().Start.Line, context.expression().Start.Column));
            }

            HasFallThrough = false;

            for (int i = 0; i < context.caseBlock().statement().Length; i++)
            {
                if (context.caseBlock().statement(i) is FallthroughStatement)
                {
                    HasFallThrough = true;
                    break;
                }
            }


        }


    }
}
