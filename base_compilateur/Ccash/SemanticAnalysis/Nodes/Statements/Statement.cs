using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.SemanticAnalysis.Nodes.Statements.Loops;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public abstract class Statement
    {
        public static Statement Create(CcashParser.StatementContext context, SymbolTable symbolTable)
        {
            switch (context)
            {
                case CcashParser.ReassignmentStatementContext reassignmentStatementContext:
                    return AssignmentOperatorToMethodCall(reassignmentStatementContext.reassignment(), symbolTable);

                case CcashParser.FunctionCallStatementContext functionCallContext:
                    return new FunctionCallStatement(functionCallContext.functionCall(), symbolTable);

                case CcashParser.VariableDeclarationStatementContext variableDeclarationContext:
                    return new VariableDeclaration(variableDeclarationContext.variableDeclaration(), symbolTable);

                case CcashParser.ConditionalStatementContext conditionalStatement:
                    return Create(conditionalStatement, symbolTable);

                case CcashParser.LoopStatementContext loopStatement:
                    return Create(loopStatement, symbolTable);

                case CcashParser.ControlFlowStatementContext controlFlowStatement:
                    return Create(controlFlowStatement, symbolTable);

                case CcashParser.BlockStatementContext blockStatementContext:
                    return new BlockStatement(blockStatementContext.block(), symbolTable);
            }

            throw new NotImplementedException();
        }

        private static Statement Create(CcashParser.ConditionalStatementContext conditionalStatement, SymbolTable symbolTable)
        {
            if (conditionalStatement.ifStatement() != null)
            {
                return new IfStatement(conditionalStatement.ifStatement(), symbolTable);
            }
            else if (conditionalStatement.switchStatement() != null)
            {
                return new SwitchStatement(conditionalStatement.switchStatement(), symbolTable);
            }

            throw new NotImplementedException();
        }

        private static Statement Create(CcashParser.LoopStatementContext loopStatement, SymbolTable symbolTable)
        {
            if (loopStatement.whileStatement() != null)
            {
                return new WhileStatement(loopStatement.whileStatement(), symbolTable);
            }

            if (loopStatement.forStatement() != null)
            {
                return new ForLoopStatement(loopStatement.forStatement(), symbolTable);
            }

            if (loopStatement.doStatement() != null)
            {
                return new DoWhileStatement(loopStatement.doStatement(), symbolTable);
            }

            if (loopStatement.repeatStatement() != null)
            {
                return new RepeatStatement(loopStatement.repeatStatement(), symbolTable);
            }
            throw new NotImplementedException();
        }

        private static Statement Create(CcashParser.ControlFlowStatementContext controlFlowStatement, SymbolTable symbolTable)
        {
            if (controlFlowStatement.returnStatement() != null)
            {
                return new ReturnStatement(controlFlowStatement.returnStatement(), symbolTable);
            }

            if (controlFlowStatement.continueStatement() != null)
            {
                return new ContinueStatement(controlFlowStatement.continueStatement(), symbolTable);
            }

            if (controlFlowStatement.breakStatement() != null)
            {
                return new BreakStatement(controlFlowStatement.breakStatement(), symbolTable);
            }
            throw new NotImplementedException();
        }

        protected static Statement AssignmentOperatorToMethodCall(CcashParser.ReassignmentContext context, SymbolTable symbolTable)
        {
            var identifier = context.Identifier(0).GetText();
            var symbolInfo = symbolTable[identifier];
            if (!symbolInfo.IsMutable)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'op�rande de gauche doit �tre mutable.", context.Identifier(0).Symbol.Line, context.Identifier(0).Symbol.Column));
            }

            var op = context.children[1].GetText();
            if (op == ":=:")
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'op�rateur d'�change n'est pas impl�ment�.", context.Identifier(0).Symbol.Line, context.Identifier(0).Symbol.Column));
            }

            var rightOperand = new ReferenceExpression(identifier, symbolTable);
            var leftOperand = Expression.Create(context.expression(), symbolTable);

            return new FunctionCallStatement(rightOperand.ReferredType, $"operator{op}", symbolTable, rightOperand, leftOperand);
        }
    }
}