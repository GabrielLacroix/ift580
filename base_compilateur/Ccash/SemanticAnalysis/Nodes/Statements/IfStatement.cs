using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public class IfStatement : Statement, IScope
    {
        public SymbolTable SymbolTable { get; }

        public Expression Expression { get; }

        public Block Block { get; }

        public List<ElseIfStatement> ElseIfStatements { get; }
        
        public object CodeGenData { get; set; }

        public IfStatement(CcashParser.IfStatementContext context, SymbolTable parentSymbolTable)
        {
            Expression = Expression.Create(context.expression(), parentSymbolTable);
            if (!(Expression.Type is BooleanType))
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit �tre de type Bool�en.", context.expression().Start.Line, context.expression().Start.Column));
            }

            ElseIfStatements =
                context.elseIfStatement()?.Select(elseIf => new ElseIfStatement(elseIf, parentSymbolTable)).ToList() ??
                new List<ElseIfStatement>();

            SymbolTable = new SymbolTable(parentSymbolTable, this);
            Block = new Block(context.block(), SymbolTable);

            if (context.elseStatement() != null)
            {
                ElseIfStatements.Add(new ElseIfStatement(context.elseStatement(), parentSymbolTable));
            }
        }
    }
    
    public class ElseIfStatement : IScope
    {
        public SymbolTable SymbolTable { get; }

        public Expression Expression { get; }

        public Block Block { get; }

        public bool IsElseStatement => Expression == null;

        public ElseIfStatement(CcashParser.ElseIfStatementContext context, SymbolTable parentSymbolTable)
        {
            if (context.expression() != null)
            {
                Expression = Expression.Create(context.expression(), parentSymbolTable);
                if (!(Expression.Type is BooleanType))
                {
                    ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit �tre de type Bool�en.", context.expression().Start.Line, context.expression().Start.Column));
                }
            }

            SymbolTable = new SymbolTable(parentSymbolTable, this);
            Block = new Block(context.block(), SymbolTable);
        }

        public ElseIfStatement(CcashParser.ElseStatementContext context, SymbolTable parentSymbolTable)
        {
            SymbolTable = new SymbolTable(parentSymbolTable, this);
            Block = new Block(context.block(), SymbolTable);
        }
    }
}