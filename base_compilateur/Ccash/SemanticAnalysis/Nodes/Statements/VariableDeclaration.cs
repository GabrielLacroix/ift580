using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public class VariableDeclaration : Statement
    {
        public string Name { get; }
        
        public CcashType Type { get; }
        
        public Expression InitializingExpression { get; }
        
        public VariableDeclaration(CcashParser.VariableDeclarationContext context, SymbolTable symbolTable)
        {
            Name = context.variable().Identifier().GetText();
            Type = CcashType.Create(context.variable().variableType());
            InitializingExpression = Expression.Create(context.expression(), symbolTable);

            if (Type != InitializingExpression.Type)
            {
                ErrorsManager.AddError(String.Format("{0}:{1} : L'expression doit �tre du m�me type que la variable.", context.Start.Line, context.Start.Column));
            }
            
            symbolTable.AddSymbol(Name, Type, context.variable().Mut() != null);
        }

        public VariableDeclaration(string name, Expression initializingExpression, bool isMutable, SymbolTable symbolTable)
        {
            Name = name;
            InitializingExpression = initializingExpression;
            Type = InitializingExpression.Type;
            
            symbolTable.AddSymbol(Name, Type, isMutable);
        }
    }
}