using Ccash.Antlr;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public class BlockStatement : Statement, IScope
    {
        public Block Block { get; }

        public SymbolTable SymbolTable { get; }
        
        public BlockStatement(CcashParser.BlockContext context, SymbolTable parentSymbolTable)
        {
            SymbolTable = new SymbolTable(parentSymbolTable, this);
            Block = new Block(context, SymbolTable);
        }
    }
}