﻿using System;
using LLVMSharp;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{

    interface Breakable
    {
        LLVMBasicBlockRef NextBlock();

    }
}
