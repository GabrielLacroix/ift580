﻿using System;
using Ccash.Antlr;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    public class EmptyStatement : Statement
    {
        public EmptyStatement() : base()
        {
        }
    }
}