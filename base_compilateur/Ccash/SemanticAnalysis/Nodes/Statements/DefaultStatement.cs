﻿using Ccash.Antlr;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ccash.SemanticAnalysis.Nodes.Statements
{
    class DefaultStatement : Statement, IScope
    {

        public SymbolTable SymbolTable { get; }

        public Block Block { get;  }

        public DefaultStatement(CcashParser.DefaultStatementContext defaultStatementContext, SymbolTable symbolTable)
        {
            SymbolTable = new SymbolTable(symbolTable, this);
            Block = new Block(defaultStatementContext.block(), symbolTable);
        }
    }
}
