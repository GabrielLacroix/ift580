using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;
using Ccash.Types;

namespace Ccash.SemanticAnalysis.Nodes
{
    public class CompilationUnit : IScope
    {
        public SymbolTable SymbolTable { get; }

        public List<ClassDeclaration> ClassDeclarations { get; }

        public List<FunctionDeclaration> FunctionDeclarations { get; }

        public CompilationUnit(CcashParser.CompilationUnitContext context)
        {
            SymbolTable = new SymbolTable(this);
            
            AddCompilerIntrinsics(SymbolTable);
            
            AddFunctionsToScope(SymbolTable, context);
            
            FunctionDeclarations = context.functionDeclaration()
                .Select(f => new FunctionDeclaration(f, SymbolTable))
                .ToList();
            
            ClassDeclarations = context.classDeclaration()
                .Select(c => new ClassDeclaration())
                .ToList();
        }

        private static void AddFunctionsToScope(SymbolTable symbolTable, CcashParser.CompilationUnitContext context)
        {
            foreach (var functionContext in context.functionDeclaration())
            {
                var identifier = functionContext.Identifier().GetText();
                var functionType = new FunctionType(functionContext);

                var functionName = FunctionNameMangler.Mangle(identifier, functionType.ParameterTypes.ToArray());

                symbolTable.AddImmutableSymbol(functionName, functionType);
            }
        }

        private static void AddCompilerIntrinsics(SymbolTable symbolTable)
        {
            foreach (var type in CcashType.AllPrimitives)
            {
                AddConstructors(type, symbolTable);
                AddMethods(type, symbolTable);
            }
        }

        private static void AddConstructors(CcashType type, SymbolTable symbolTable)
        {
            foreach (var constructor in type.Constructors)
            {
                var functionName = FunctionNameMangler.Mangle(type.Name, constructor.ParameterTypes.ToArray());
                symbolTable.AddImmutableSymbol(functionName, constructor);
            }
        }

        private static void AddMethods(CcashType type, SymbolTable symbolTable)
        {
            foreach (var method in type.Methods)
            {
                symbolTable.AddImmutableSymbol(method.FullName, method.FunctionType);
            }
        }
    }
}