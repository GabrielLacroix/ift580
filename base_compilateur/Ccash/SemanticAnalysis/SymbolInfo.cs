using Ccash.Types;

namespace Ccash.SemanticAnalysis
{
    public class SymbolInfo
    {
        public CcashType Type { get; }

        public bool IsMutable { get; }
        
        public object CodeGeneratorData { get; set; }

        public SymbolInfo(CcashType type, bool isMutable)
        {
            Type = type;
            IsMutable = isMutable;
        }
    }
}