﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ccash
{
    public static class ErrorsManager
    {
        public static bool HasErrors => Errors.Count > 0;

        public static IList<String> Errors { get; }

        static ErrorsManager()
        {
            Errors = new List<String>();
        }

        public static void AddError(string error)
        {
            Errors.Add(error);
        }
    }
}
