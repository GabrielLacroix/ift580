using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.CodeGeneration.LLVMGenerator.Intrinsics;
using Ccash.SemanticAnalysis;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using Ccash.SemanticAnalysis.Nodes.Statements;
using Ccash.SemanticAnalysis.Nodes.Statements.Loops;
using Ccash.Types;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator
{
    public class StatementGenerator
    {
        private uint _ifCounter;

        private uint _whileCounter;

        private uint _forCounter;

        private uint _doWhileCounter;

        private uint _repeatCounter;

        private uint _switchCounter;


        private ExpressionGenerator ExpressionGenerator { get; }

        private IIRBuilder Builder { get; }

        private LLVMValueRef Printf { get; }

        public StatementGenerator(IIRBuilder builder, LLVMValueRef printf)
        {
            Builder = builder;
            Printf = printf;
            ExpressionGenerator = new ExpressionGenerator(Builder);
        }

        public void ResetCounters()
        {
            _ifCounter = 0;
            _forCounter = 0;
            _whileCounter = 0;
            _doWhileCounter = 0;
            _repeatCounter = 0;
            _switchCounter = 0;
        }

        public void Generate(Statement statement, SymbolTable symbolTable)
        {
            switch (statement)
            {
                case FunctionCallStatement functionCallStatement:
                    GenerateFunctionCall(functionCallStatement, symbolTable);
                    break;

                case ReturnStatement returnStatement:
                    GenerateReturn(returnStatement, symbolTable);
                    break;

                case ContinueStatement continueStatement:
                    GenerateContinue(continueStatement, symbolTable);
                    break;

                case BreakStatement breakStatement:
                    GenerateBreak(breakStatement, symbolTable);
                    break;

                case IfStatement ifStatement:
                    GenerateIf(ifStatement, symbolTable);
                    break;

                case WhileStatement whileStatement:
                    GenerateWhile(whileStatement, symbolTable);
                    break;

                case ForLoopStatement forLoopStatement:
                    GenerateFor(forLoopStatement);
                    break;

                case DoWhileStatement doWhileStatement:
                    GenerateDoWhile(doWhileStatement, symbolTable);
                    break;

                case RepeatStatement repeatStatement:
                    GenerateRepeat(repeatStatement, symbolTable);
                    break;

                case VariableDeclaration variableStatement:
                    GenerateVariableDeclaration(variableStatement, symbolTable);
                    break;

                case BlockStatement blockStatement:
                    blockStatement.Block.Statements.ForEach(s => Generate(s, blockStatement.SymbolTable));
                    break;

                case SwitchStatement switchStatement:
                    GenerateSwitchStatement(switchStatement, symbolTable);
                    break;

                default:
                    throw new NotImplementedException($"{statement.GetType()} is not yet supported");
            }
        }

        private void GenerateFunctionCall(FunctionCallStatement functionCallStatement, SymbolTable symbolTable)
        {
            var args = functionCallStatement.Arguments.Select(a => ExpressionGenerator.Generate(a, symbolTable)).ToArray();
            var functionName = functionCallStatement.FunctionName;

            if (IntrinsicStatementGenerator.IsIntrinsicStatement(functionName))
            {
                var operandTypes = functionCallStatement.Arguments.Select(a => a.Type).ToArray();
                IntrinsicStatementGenerator.GenerateAssignmentCall(Builder, args, operandTypes, functionName);
            }
            else if (functionName == "printf")
            {
                Builder.CallFunction(Printf, args);
            }
            else
            {
                Builder.CallFunction(symbolTable.GetCodeGeneratorData<LLVMValueRef>(functionName), args);
            }
        }

        private void GenerateReturn(ReturnStatement returnStatement, SymbolTable symbolTable)
        {
            if (returnStatement.Expression == null)
            {
                Builder.ReturnVoid();
            }
            else
            {
                LLVMValueRef expression = ExpressionGenerator.Generate(returnStatement.Expression, symbolTable);
                Builder.Return(expression);
            }
        }

        private void GenerateContinue(ContinueStatement continueStatement, SymbolTable symbolTable)
        {
            LoopStatement Parent = continueStatement.Parent;
            LLVMBasicBlockRef nextBlock = (LLVMBasicBlockRef)Parent.ExpressionBlock;
            Builder.Branch(nextBlock);
        }

        private void GenerateBreak(BreakStatement breakStatement, SymbolTable symbolTable)
        {
            Breakable Parent = breakStatement.Parent;
            LLVMBasicBlockRef nextBlock = Parent.NextBlock();
            
            Builder.Branch(nextBlock);
        }

        private void GenerateVariableDeclaration(VariableDeclaration variableDeclaration, SymbolTable symbolTable)
        {
            LLVMTypeRef type = TypeResolver.Resolve(variableDeclaration.Type);
            LLVMValueRef variable = Builder.AllocateVariable(type, variableDeclaration.Name);
            LLVMValueRef initExpression = ExpressionGenerator.Generate(variableDeclaration.InitializingExpression, symbolTable);

            Builder.Store(initExpression, variable);
            symbolTable[variableDeclaration.Name].CodeGeneratorData = variable;
        }

        private void GenerateWhile(WhileStatement whileStatement, SymbolTable symbolTable)
        {
            var id = _whileCounter++;
            
            var conditionExpressionBlock = InsertBlockAfterCurrentBlock($"while{id}Condition");
            var loopBodyBlock = InsertBlockAfter(conditionExpressionBlock, $"while{id}Body");
            var nextBlock = InsertBlockAfter(loopBodyBlock, $"while{id}Next");

            whileStatement.NextBlock = nextBlock;

            whileStatement.ExpressionBlock = conditionExpressionBlock;

            Builder.Branch(conditionExpressionBlock);

            Builder.PositionAtEnd(conditionExpressionBlock);
            LLVMValueRef cond = ExpressionGenerator.Generate(whileStatement.Expression, symbolTable);
            Builder.ConditionalBranch(cond, loopBodyBlock, nextBlock);

            GenerateStatementsInBlock(loopBodyBlock, whileStatement.Block.Statements, whileStatement.SymbolTable, conditionExpressionBlock);

            Builder.PositionAtEnd(nextBlock);
        }

        private void GenerateFor(ForLoopStatement forLoop)
        {
            var id = _forCounter++;
            
            var conditionExpressionBlock = InsertBlockAfterCurrentBlock($"for{id}Condition");
            var loopBodyBlock = InsertBlockAfter(conditionExpressionBlock, $"for{id}Body");
            var nextBlock = InsertBlockAfter(loopBodyBlock, $"for{id}Next");

            forLoop.NextBlock = nextBlock;

            forLoop.ExpressionBlock = conditionExpressionBlock;

            if (forLoop.Declaration != null)
            {
                Generate(forLoop.Declaration, forLoop.SymbolTable);
            }

            Builder.Branch(conditionExpressionBlock);

            Builder.PositionAtEnd(conditionExpressionBlock);
            LLVMValueRef cond = ExpressionGenerator.Generate(forLoop.Expression, forLoop.SymbolTable);
            Builder.ConditionalBranch(cond, loopBodyBlock, nextBlock);

            Builder.PositionAtEnd(loopBodyBlock);
            forLoop.Block.Statements.ForEach(s => Generate(s, forLoop.SymbolTable));
            if (forLoop.Assignment != null)
            {
                Generate(forLoop.Assignment, forLoop.SymbolTable);
            }
            if (!BlockHasTerminator(Builder.CurrentBlock))
            {
                Builder.Branch(conditionExpressionBlock);
            }

            Builder.PositionAtEnd(nextBlock);

        }

        private void GenerateRepeat(RepeatStatement repeatStatement, SymbolTable symbolTable)
        {
            // Copier/coller de GenerateWhile
            var id = _repeatCounter++;

            //Pas mal c/c du for
            var conditionExpressionBlock = InsertBlockAfterCurrentBlock($"repeat{id}Condition");
            var loopBodyBlock = InsertBlockAfter(conditionExpressionBlock, $"repeat{id}Body");
            var nextBlock = InsertBlockAfter(loopBodyBlock, $"repeat{id}Next");

            repeatStatement.NextBlock = nextBlock;

            repeatStatement.ExpressionBlock = conditionExpressionBlock;

            LLVMTypeRef type = TypeResolver.Resolve(new IntegerType("uint32"));

            LLVMValueRef iterMutVar = Builder.AllocateVariable(type, $"repeat{id}iterator");
            LLVMValueRef iterVal = LLVM.ConstInt(type, 0, true);
            Builder.Store(iterVal, iterMutVar);

            LLVMValueRef acc = Builder.AllocateVariable(type, $"repeat{id}acc");
            LLVMValueRef accVarVal = LLVM.ConstInt(type, 1, true);
            Builder.Store(accVarVal, acc);

            LLVMValueRef end = ExpressionGenerator.Generate(repeatStatement.Expression, repeatStatement.SymbolTable);
            LLVMValueRef endRef = Builder.AllocateVariable(type, $"repeat{id}end");
            Builder.Store(end, endRef);


            Builder.Branch(conditionExpressionBlock);

            Builder.PositionAtEnd(conditionExpressionBlock);


            LLVMValueRef cond = Builder.NE(Builder.Load(iterMutVar), end);
            Builder.ConditionalBranch(cond, loopBodyBlock, nextBlock);

            Builder.PositionAtEnd(loopBodyBlock);
            repeatStatement.Block.Statements.ForEach(s => Generate(s, repeatStatement.SymbolTable));
            

            LLVMValueRef result = Builder.Add(Builder.Load(iterMutVar), Builder.Load(acc));
            Builder.Store(result, iterMutVar);

            

            if (!BlockHasTerminator(Builder.CurrentBlock))
            {
                Builder.Branch(conditionExpressionBlock);
            }

            Builder.PositionAtEnd(nextBlock);
        }

        private void GenerateDoWhile(DoWhileStatement doWhileStatement, SymbolTable symbolTable)
        {
            // Copier/coller de GenerateWhile
            var id = _doWhileCounter++;
            
            // J'ai inverse l'ordre des InsertBlock
            var loopBodyBlock = InsertBlockAfterCurrentBlock($"dowhile{id}Body");
            var conditionExpressionBlock = InsertBlockAfter(loopBodyBlock, $"dowhile{id}Condition");
            var nextBlock = InsertBlockAfter(conditionExpressionBlock, $"dowhile{id}Next");

            doWhileStatement.NextBlock = nextBlock;

            doWhileStatement.ExpressionBlock = conditionExpressionBlock;

            Builder.Branch(loopBodyBlock);

            Builder.PositionAtEnd(conditionExpressionBlock);
            LLVMValueRef cond = ExpressionGenerator.Generate(doWhileStatement.Expression, symbolTable);
            Builder.ConditionalBranch(cond, loopBodyBlock, nextBlock);

            GenerateStatementsInBlock(loopBodyBlock, doWhileStatement.Block.Statements, doWhileStatement.SymbolTable, conditionExpressionBlock);

            Builder.PositionAtEnd(nextBlock);
        }


        private void GenerateIf(IfStatement ifStatement, SymbolTable symbolTable)
        {
            var ifId = $"if{_ifCounter++}";

            var thenBlock = InsertBlockAfterCurrentBlock($"{ifId}Then");
            var elseBlock = InsertBlockAfter(thenBlock, $"{ifId}Else");
            var nextBlock = InsertBlockAfter(elseBlock, $"{ifId}Next");

            LLVMValueRef cond = ExpressionGenerator.Generate(ifStatement.Expression, symbolTable);
            Builder.ConditionalBranch(cond, thenBlock, elseBlock);

            GenerateStatementsInBlock(thenBlock, ifStatement.Block.Statements, ifStatement.SymbolTable, nextBlock);

            Builder.PositionAtEnd(elseBlock);
            GenerateElseIfStatements(ifStatement.ElseIfStatements, ifId, nextBlock, symbolTable);

            Builder.PositionAtEnd(nextBlock);
        }

        private void GenerateElseIfStatements(List<ElseIfStatement> elseIfStatements, string ifId, LLVMBasicBlockRef nextBlock, SymbolTable symbolTable)
        {
            var elseIfCounter = 0;
            foreach (var elseIfStatement in elseIfStatements)
            {
                var elseIfId = $"{ifId}ElseIf{elseIfCounter++}";

                var thenBlock = InsertBlockAfterCurrentBlock($"{elseIfId}Then");
                var elseBlock = InsertBlockAfter(thenBlock, $"{elseIfId}Else");

                if (elseIfStatement.IsElseStatement)
                {
                    Builder.Branch(thenBlock);
                }
                else
                {
                    var cond = ExpressionGenerator.Generate(elseIfStatement.Expression, symbolTable);
                    Builder.ConditionalBranch(cond, thenBlock, elseBlock);
                }

                GenerateStatementsInBlock(thenBlock, elseIfStatement.Block.Statements, elseIfStatement.SymbolTable, nextBlock);

                Builder.PositionAtEnd(elseBlock);
            }
            
            if (!BlockHasTerminator(Builder.CurrentBlock))
            {
                Builder.Branch(nextBlock);
            }
        }

        private void GenerateSwitchStatement(SwitchStatement switchStatement, SymbolTable symbolTable)
        {

            var switchId = $"switch{_switchCounter++}";
            var nextBlock = InsertBlockAfterCurrentBlock($"{switchId}Next");
            switchStatement.NextBlock = nextBlock;
            uint caseCounter = 0;

            var switchVar = Builder.AllocateVariable(TypeResolver.Resolve(switchStatement.Expression.Type), $"{switchId}Var");

            List<LLVMValueRef> conds = new List<LLVMValueRef>();
            uint index = 0;
            foreach (CaseStatement caseStatement in switchStatement.CaseStatements)
            {
                index++;
                conds.Add(GenerateCaseStatement(index != switchStatement.CaseStatements.Count(), switchVar, switchStatement, caseStatement, switchId, caseCounter++, switchStatement.NextBlock, symbolTable));
            }
            //Default

            if (switchStatement.DefaultStatement != null)
            {
                var finalCond = conds[0];
                foreach (var cond in conds)
                {
                    finalCond = Builder.Or(finalCond, cond);
                }


                GenerateDefaultStatement(switchStatement.DefaultStatement, symbolTable, switchId, Builder.Neg(finalCond), switchStatement.NextBlock);
            }

            Builder.PositionAtEnd(switchStatement.NextBlock);
        }


        private LLVMValueRef GenerateCaseStatement(bool hasNext, LLVMValueRef switchVar, SwitchStatement switchStatement, CaseStatement caseStatement, string switchId, uint caseCounter, LLVMBasicBlockRef nextBlock, SymbolTable symbolTable)
        {
            //Nous avons eu un probl�me o� le nom du prochain case est repete plusieurs fois.
            //Comme par exemple switch0Case1 pointe vers switch0Case2, mais le prochain case se nomme switch0Case22.
            //Nous n'avons pas �t� capable de trouver la source du probl�me.
            //Le probleme est clairement que nous faisons InsertBlockAfter.
            var currCase = InsertBlockAfterCurrentBlock(switchId + "Case" + caseCounter);
            var currBody = InsertBlockAfter(currCase, switchId + "Case" + caseCounter + "body");

            LLVMBasicBlockRef nextCase;
            if (hasNext)
            {
                nextCase = InsertBlockAfter(currCase, switchId + "Case" + (caseCounter + 1).ToString());
            }
            else if(switchStatement.DefaultStatement != null)
            {
                nextCase = InsertBlockAfter(currCase, $"{switchId}Default");
            }
            else
            {
                nextCase = nextBlock;
            }
            Builder.PositionAtEnd(currCase);

            var caseVar = Builder.AllocateVariable(TypeResolver.Resolve(switchStatement.Expression.Type), switchId + "Case" + caseCounter + "Var");

            var cond = Builder.EQ(switchVar, caseVar);
            Builder.ConditionalBranch(cond, currBody, nextCase);
            Builder.PositionAtEnd(currBody);
            caseStatement.CaseBlock.Statements.ForEach(s => Generate(s, caseStatement.SymbolTable));

            //Builder.PositionAtEnd(nextCase);
            

            if ((switchStatement.HasFallThrough || caseStatement.HasFallThrough) && !BlockHasTerminator(Builder.CurrentBlock))
            {
                Builder.Branch(nextCase);
                
            }
            else if(!BlockHasTerminator(Builder.CurrentBlock))
            {
                Builder.Branch(nextBlock); 
                
            }
            return cond;
            
        }

        private void GenerateDefaultStatement(DefaultStatement defaultStatement, SymbolTable symbolTable, string switchId, LLVMValueRef cond, LLVMBasicBlockRef nextBlock)
        {
            var id = $"{switchId}Default";

            var currBlock = InsertBlockAfterCurrentBlock(id);

            Builder.ConditionalBranch(cond, currBlock, nextBlock);
            Builder.PositionAtEnd(currBlock);
            GenerateStatementsInBlock(currBlock, defaultStatement.Block.Statements.OfType<Statement>().ToList(), defaultStatement.SymbolTable, nextBlock);
            Builder.Branch(nextBlock);

        }

        private void GenerateStatementsInBlock(LLVMBasicBlockRef block, List<Statement> statements, SymbolTable symbolTable, LLVMBasicBlockRef nextBlock)
        {
            Builder.PositionAtEnd(block);
            statements.ForEach(s => Generate(s, symbolTable));
            if (!BlockHasTerminator(Builder.CurrentBlock))
            {
                Builder.Branch(nextBlock);
            }
        }
        
        private static bool BlockHasTerminator(LLVMBasicBlockRef block)
        {
            return block.GetBasicBlockTerminator().Pointer != IntPtr.Zero;
        }

        private LLVMBasicBlockRef InsertBlockAfterCurrentBlock(string blockName)
        {
            return InsertBlockAfter(Builder.CurrentBlock, blockName);
        }

        private static LLVMBasicBlockRef InsertBlockAfter(LLVMBasicBlockRef block, string blockName)
        {
            var nextBlock = block.GetNextBasicBlock();

            if (nextBlock.Pointer == IntPtr.Zero)
            {
                var function = block.GetBasicBlockParent();
                return LLVM.AppendBasicBlock(function, blockName);
            }

            return InsertBlockBefore(nextBlock, blockName);
        }

        private static LLVMBasicBlockRef InsertBlockBefore(LLVMBasicBlockRef block, string blockName)
        {
            return block.InsertBasicBlock(blockName);
        }
    }
}