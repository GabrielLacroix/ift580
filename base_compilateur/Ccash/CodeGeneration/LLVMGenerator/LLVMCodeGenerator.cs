﻿using System;
using System.IO;
using System.Linq;
using Ccash.SemanticAnalysis;
using Ccash.SemanticAnalysis.Nodes;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator
{
    public class LLVMCodeGenerator
    {
        private LLVMModuleRef Module { get; } = LLVM.ModuleCreateWithName("my_module");

        private IIRBuilder Builder { get; } = new IRBuilder();

        private StatementGenerator StatementGenerator { get; }

        public LLVMCodeGenerator()
        {
            var charPointerType = LLVM.PointerType(LLVMTypeRef.Int8Type(), 0);
            var printfType = LLVM.FunctionType(LLVM.Int32Type(), new[] { charPointerType }, true);
            var printf = LLVM.AddFunction(Module, "printf", printfType);
            LLVM.SetLinkage(printf, LLVMLinkage.LLVMExternalLinkage);
            
            StatementGenerator = new StatementGenerator(Builder, printf);
        }

        public void Generate(AbstractSyntaxTree ast, string outputName) 
        {
            ast.CompilationUnit.FunctionDeclarations.ForEach(f => GenerateFunctionHeader(f, ast.CompilationUnit.SymbolTable));
            ast.CompilationUnit.FunctionDeclarations.ForEach(GenerateFunctionDeclaration);

            LLVM.PrintModuleToFile(Module, outputName + ".ir", out _);
            LLVM.WriteBitcodeToFile(Module, outputName  + ".bc");
        }

        private void GenerateFunctionHeader(FunctionDeclaration functionDeclaration, SymbolTable symbolTable)
        {
            var returnType = TypeResolver.Resolve(functionDeclaration.FunctionType.ReturnType);
            var parameterTypes = functionDeclaration.Parameters.Select(p => TypeResolver.Resolve(p.Type)).ToArray();

            var functionType = LLVM.FunctionType(returnType, parameterTypes, false);
            var function = LLVM.AddFunction(Module, functionDeclaration.Name, functionType);

            symbolTable[functionDeclaration.Name].CodeGeneratorData = function;
        }

        private void GenerateFunctionDeclaration(FunctionDeclaration functionDeclaration)
        {
            var function = functionDeclaration.SymbolTable.GetCodeGeneratorData<LLVMValueRef>(functionDeclaration.Name);

            for (var i = 0; i < functionDeclaration.Parameters.Count; i++)
            {
                var paramName = functionDeclaration.Parameters[i].Name;
                var param = LLVM.GetParam(function, (uint)i);
                LLVM.SetValueName(param, paramName);

                functionDeclaration.SymbolTable[paramName].CodeGeneratorData = param;
            }

            var entry = LLVM.AppendBasicBlock(function, "entry");
            Builder.PositionAtEnd(entry);

            functionDeclaration.Block.Statements.ForEach(s => StatementGenerator.Generate(s, functionDeclaration.SymbolTable));

            if (function.GetLastBasicBlock().GetBasicBlockTerminator().Pointer == IntPtr.Zero)
            {
                Builder.ReturnVoid();
            }
            
            var result = LLVM.VerifyFunction(function, LLVMVerifierFailureAction.LLVMPrintMessageAction);
            if (result.Value != 0)
            {
                //File.WriteAllText(functionDeclaration.Name + ".txt", function.ToString());
                Console.WriteLine($"\tIn function {functionDeclaration.Name}");
                Console.WriteLine(function.ToString());
            }
            
            StatementGenerator.ResetCounters();
        }
    }
}