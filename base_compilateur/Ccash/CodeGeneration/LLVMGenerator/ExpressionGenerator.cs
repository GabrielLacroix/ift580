using System;
using System.Linq;
using Ccash.CodeGeneration.LLVMGenerator.Intrinsics;
using Ccash.SemanticAnalysis;
using Ccash.SemanticAnalysis.Nodes.Expressions;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator
{
    public class ExpressionGenerator
    {
        private IIRBuilder Builder { get; }

        public ExpressionGenerator(IIRBuilder builder)
        {
            Builder = builder;
        }
        
        public LLVMValueRef Generate(Expression expression, SymbolTable symbolTable)
        {
            switch (expression)
            {
                case IntegerLiteralExpression intLiteralExpression:
                {
                    LLVMTypeRef type = TypeResolver.Resolve(intLiteralExpression.Type);
                    return LLVM.ConstInt(type, (ulong) intLiteralExpression.Value, true);
                }

                case FloatLiteralExpression floatLiteralExpression:
                {
                    LLVMTypeRef type = TypeResolver.Resolve(floatLiteralExpression.Type);
                    return LLVM.ConstReal(type, floatLiteralExpression.Value);
                }

                case StringLiteralExpression strLiteralExpression:
                {
                    LLVMValueRef str = Builder.AddGlobalString("str", strLiteralExpression.Value);
                    return Builder.ReinterpretCast(str, LLVM.PointerType(LLVMTypeRef.Int8Type(), 0));
                }

                case BooleanLiteralExpression boolLiteralExpression:
                {
                    LLVMTypeRef type = TypeResolver.Resolve(boolLiteralExpression.Type);
                    return LLVM.ConstInt(type, Convert.ToByte(boolLiteralExpression.Value), false);
                }

                case IdentifierExpression identifierExpression:
                {
                    var identifier = identifierExpression.Identifier;
                    var variable = symbolTable.GetCodeGeneratorData<LLVMValueRef>(identifier);
                    return symbolTable.IsSymbolAFunctionParameter(identifier) ? variable : Builder.Load(variable);
                }

                case ReferenceExpression referenceExpression:
                {
                    var identifier = referenceExpression.Identifier;
                    return symbolTable.GetCodeGeneratorData<LLVMValueRef>(identifier);
                }

                case FunctionCallExpression functionCallExpression:
                {
                    var functionName = functionCallExpression.FunctionName;

                    LLVMValueRef[] args = functionCallExpression.Arguments.Select(e => Generate(e, symbolTable)).ToArray();

                    if (IntrinsicExpressions.IsIntrinsicFunction(functionName))
                    {
                        return IntrinsicExpressions.GenerateIntrinsicCall(Builder, args,
                            functionCallExpression.Arguments.Select(a => a.Type).ToArray(), functionName);
                    }

                    var function = symbolTable.GetCodeGeneratorData<LLVMValueRef>(functionName);
                    return Builder.CallFunction(function, args);
                }

                case TernaryExpression ternaryExpression:
                {
                    LLVMValueRef condition = Generate(ternaryExpression.ConditionExpression, symbolTable);
                    LLVMValueRef trueValue = Generate(ternaryExpression.TrueExpression, symbolTable);
                    LLVMValueRef falseValue = Generate(ternaryExpression.FalseExpression, symbolTable);

                    return Builder.Ternary(condition, trueValue, falseValue);
                }
                
                default:
                    throw new NotImplementedException($"{expression.GetType()} is not yet supported");
            }
        }
    }
}