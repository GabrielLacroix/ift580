using System;
using System.Collections.Generic;
using Ccash.Types;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator
{
    public static class TypeResolver
    {
        private static readonly Dictionary<string, LLVMTypeRef> TypeTable = new Dictionary<string, LLVMTypeRef>
        {
            { "int8", LLVM.Int8Type() },
            { "uint8", LLVM.Int8Type() },
            { "int16", LLVM.Int16Type() },
            { "uint16", LLVM.Int16Type() },
            { "int32", LLVM.Int32Type() },
            { "uint32", LLVM.Int32Type() },
            { "int64", LLVM.Int64Type() },
            { "uint64", LLVM.Int64Type() },
            { "float32", LLVM.FloatType() },
            { "float64", LLVM.DoubleType() },
            { "bool", LLVM.Int1Type() },
            { "string", LLVM.PointerType(LLVMTypeRef.Int8Type(), 0)}
        };
        
        public static LLVMTypeRef Resolve(CcashType type)
        {
            if (type == CcashType.Void)
            {
                return LLVM.VoidType();
            }
            
            if (TypeTable.TryGetValue(type.Name, out var typeRef))
            {
                return typeRef;
            }

            throw new NotImplementedException($"Unknown type: {type.Name}");
        }
    }
}