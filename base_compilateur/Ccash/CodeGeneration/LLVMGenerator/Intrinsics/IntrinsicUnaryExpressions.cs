using System;
using Ccash.Types;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator.Intrinsics
{
    public static class IntrinsicUnaryExpressions
    {
        public static LLVMValueRef GenerateIntrinsicCall(IIRBuilder builder, LLVMValueRef operand,
            CcashType operandType, string symbol)
        {
            switch (symbol)
            {
                case "!":
                    return builder.BitNot(operand);
                case "-":
                    return operandType.IsIntegral
                        ? builder.Neg(operand)
                        : builder.FNeg(operand);
                case "bool":
                    return GenerateBoolConstructorCall(builder, operand, operandType);
                case "int8":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Int8);
                case "uint8":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Uint8);
                case "int16":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Int16);
                case "uint16":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Uint16);
                case "int32":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Int32);
                case "uint32":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Uint32);
                case "int64":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Int64);
                case "uint64":
                    return GenerateIntConstructorCall(builder, operand, operandType, CcashType.Uint64);
                case "float32":
                    return GenerateFloatConstructorCall(builder, operand, operandType, CcashType.Float32);
                case "float64":
                    return GenerateFloatConstructorCall(builder, operand, operandType, CcashType.Float64);

                default: throw new NotImplementedException();
            }
        }

        private static LLVMValueRef GenerateBoolConstructorCall(IIRBuilder builder, LLVMValueRef operand, CcashType sourceType)
        {
            if (sourceType.IsIntegral)
            {
                var zeroInt = LLVM.ConstInt(TypeResolver.Resolve(sourceType), 0, true);
                return builder.NE(operand, zeroInt);
            }

            var zeroFloat = LLVM.ConstReal(TypeResolver.Resolve(sourceType), 0);
            return builder.FNE(operand, zeroFloat);
        }

        private static LLVMValueRef GenerateIntConstructorCall(IIRBuilder builder, LLVMValueRef operand, CcashType sourceType, IntegerType destinationType)
        {
            var llvmDestinationType = TypeResolver.Resolve(destinationType);

            switch (sourceType)
            {
                case IntegerType sourceIntType when destinationType.Size > sourceIntType.Size:
                    return sourceIntType.IsSigned
                        ? builder.SignExtendOrCastInt(operand, llvmDestinationType)
                        : builder.ZeroExtendOrCastInt(operand, llvmDestinationType);
                case IntegerType _:
                    return builder.TruncateOrCastInt(operand, llvmDestinationType);
                case BooleanType _:
                    return builder.CastInt(operand, llvmDestinationType);
                case FloatType _:
                    return destinationType.IsSigned
                        ? builder.FloatToSigned(operand, llvmDestinationType)
                        : builder.FloatToUnsigned(operand, llvmDestinationType);
                default:
                    throw new NotImplementedException();
            }
        }

        private static LLVMValueRef GenerateFloatConstructorCall(IIRBuilder builder, LLVMValueRef operand, CcashType sourceType, FloatType destinationType)
        {
            var llvmDestinationType = TypeResolver.Resolve(destinationType);

            switch (sourceType)
            {
                case IntegerType sourceIntType:
                    return sourceIntType.IsSigned
                        ? builder.SignedToFloat(operand, llvmDestinationType)
                        : builder.UnsignedToFloat(operand, llvmDestinationType);
                case BooleanType _:
                    return builder.UnsignedToFloat(operand, llvmDestinationType);
                case FloatType sourceFloatType:
                    return destinationType.Size > sourceFloatType.Size
                        ? builder.ExtendFloat(operand, llvmDestinationType)
                        : builder.TruncateFloat(operand, llvmDestinationType);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}