﻿using System;
using System.Collections.Generic;
using Ccash.Types;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator.Intrinsics
{
    public static class IntrinsicStatementGenerator
    {
        public static bool IsIntrinsicStatement(string func) => Functions.ContainsKey(func);

        private static Dictionary<string, string> Functions { get; }

        static IntrinsicStatementGenerator()
        {
            Functions = new Dictionary<string, string>();
            AddAssignmentOperators(Functions);
            AddArithmeticAssignmentOperators(Functions);
            AddIntegralAssignmentOperators(Functions);
        }

        public static void GenerateAssignmentCall(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes, string functionName)
        {
            var refType = ((ReferenceType) operandTypes[0]).Type;
            var symbol = Functions[functionName];
            LLVMValueRef val = builder.Load(operands[0]);

            switch (symbol)
            {
                case ":=":
                {
                    val = operands[1];
                    break;
                }
                case ":=:":
                {
                    LLVMValueRef swapVal = builder.Load(operands[1]);
                    builder.Store(val, operands[1]);
                    val = swapVal;
                    break;
                }
                case "+=":
                {
                    val = refType.IsIntegral
                        ? builder.Add(val, operands[1])
                        : builder.FAdd(val, operands[1]);
                    break;
                }
                case "-=":
                {
                    val = refType.IsIntegral
                        ? builder.Sub(val, operands[1])
                        : builder.FSub(val, operands[1]);
                    break;
                }
                case "*=":
                {
                    val = refType.IsIntegral
                        ? builder.Mul(val, operands[1])
                        : builder.FMul(val, operands[1]);
                    break;
                }
                case "/=":
                {
                    var type = refType;
                    if (type is IntegerType integer)
                    {
                        val = integer.IsSigned
                            ? builder.SignedDiv(val, operands[1])
                            : builder.UnsignedDiv(val, operands[1]);
                        break;
                    }

                    val = builder.FDiv(val, operands[1]);
                    break;
                }
                case "%=":
                {
                    val = ((IntegerType) refType).IsSigned
                        ? builder.SignedRem(val, operands[1])
                        : builder.UnsignedRem(val, operands[0]);
                    break;
                }
                case "|=":
                {
                    val = builder.Or(val, operands[1]);
                    break;
                }

                case "&=":
                {
                    val = builder.And(val, operands[1]);
                    break;
                }

                case "^=":
                {
                    val = builder.Xor(val, operands[1]);
                    break;
                }
                default:
                    throw new NotImplementedException();
            }

            builder.Store(val, operands[0]);
        }

        private static void AddAssignmentOperators(Dictionary<string, string> intrinsics)
        {
            foreach (var type in CcashType.AllPrimitives)
            {
                var reference = new ReferenceType(type, true);
                var functionName = FunctionNameMangler.Mangle($"{type.Name}::operator:=", reference, type);
                intrinsics.Add(functionName, ":=");
                functionName = FunctionNameMangler.Mangle($"{type.Name}::operator:=:", reference, reference);
                intrinsics.Add(functionName, ":=:");
            }
        }

        private static void AddArithmeticAssignmentOperators(Dictionary<string, string> intrinsics)
        {
            foreach (var op in new[] {"+=", "-=", "/=", "*="})
            {
                foreach (var intType in CcashType.IntegerTypes)
                {
                    var reference = new ReferenceType(intType, true);
                    var functionName = FunctionNameMangler.Mangle($"{intType.Name}::operator{op}", reference, intType);
                    intrinsics.Add(functionName, op);
                }

                foreach (var floatType in CcashType.FloatTypes)
                {
                    var reference = new ReferenceType(floatType, true);
                    var functionName = FunctionNameMangler.Mangle($"{floatType.Name}::operator{op}", reference, floatType);
                    intrinsics.Add(functionName, op);
                }
            }
        }

        private static void AddIntegralAssignmentOperators(Dictionary<string, string> intrinsics)
        {
            foreach (var op in new[] {"&=", "|=", "^=", "%="})
            {
                ReferenceType referenceType;
                string functionName;
                foreach (var intType in CcashType.IntegerTypes)
                {
                    referenceType = new ReferenceType(intType, true);
                    functionName = FunctionNameMangler.Mangle($"{intType.Name}::operator{op}", referenceType, intType);
                    intrinsics.Add(functionName, op);
                }

                var boolType = CcashType.Boolean;
                referenceType = new ReferenceType(boolType, true);
                functionName = FunctionNameMangler.Mangle($"{boolType.Name}::operator{op}", referenceType, boolType);
                intrinsics.Add(functionName, op);
            }
        }
    }
}