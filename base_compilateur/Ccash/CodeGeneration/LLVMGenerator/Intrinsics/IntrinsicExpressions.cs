using System;
using System.Collections.Generic;
using Ccash.Types;
using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator.Intrinsics
{
    public static class IntrinsicExpressions
    {
        public static bool IsIntrinsicFunction(string func) => Functions.ContainsKey(func);

        private static Dictionary<string, string> Functions { get; }

        static IntrinsicExpressions()
        {
            Functions = new Dictionary<string, string>();

            foreach (var type in CcashType.AllPrimitives)
            {
                AddOperators(type);
                AddConstructors(type);
            }
        }

        public static LLVMValueRef GenerateIntrinsicCall(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes, string functionName)
        {
            var symbol = Functions[functionName];

            if (operands.Length == 1)
            {
                return IntrinsicUnaryExpressions.GenerateIntrinsicCall(builder, operands[0], operandTypes[0], symbol);
            }

            switch (symbol)
            {
                case "+":
                    return GenerateAddition(builder, operands, operandTypes);
                case "-":
                    return GenerateSubtraction(builder, operands, operandTypes);
                case "*":
                    return GenerateMultiplication(builder, operands, operandTypes);
                case "/":
                    return GenerateDivision(builder, operands, operandTypes);
                case "%":
                    return GenerateModulo(builder, operands, operandTypes);
                case "^":
                    return builder.Xor(operands[0], operands[1]);
                case "||":
                case "|":
                    return builder.Or(operands[0], operands[1]);
                case "&&":
                case "&":
                    return builder.And(operands[0], operands[1]);
                case "<":
                    return GenerateLessThan(builder, operands, operandTypes);
                case ">":
                    return GenerateGreaterThan(builder, operands, operandTypes);
                case "<=":
                    return GenerateLessOrEqual(builder, operands, operandTypes);
                case ">=":
                    return GenerateGreaterOrEqual(builder, operands, operandTypes);
                case "==":
                    return GenerateEqual(builder, operands, operandTypes);
                case "!=":
                    return GenerateNotEqual(builder, operands, operandTypes);

                default: throw new NotImplementedException();
            }
        }

        private static LLVMValueRef GenerateAddition(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];
            var rightOperandType = operandTypes[1];

            if (leftOperandType != rightOperandType)
            {
                // case int + float, need to cast the int to float
                leftOperand = IntrinsicUnaryExpressions.GenerateIntrinsicCall(builder, leftOperand,
                    leftOperandType, rightOperandType.Name);
            }

            return rightOperandType.IsIntegral ? builder.Add(leftOperand, rightOperand) : builder.FAdd(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateSubtraction(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];
            var rightOperandType = operandTypes[1];

            if (leftOperandType != rightOperandType)
            {
                // case int - float, need to cast the int to float
                leftOperand = IntrinsicUnaryExpressions.GenerateIntrinsicCall(builder, leftOperand,
                    leftOperandType, rightOperandType.Name);
            }

            return rightOperandType.IsIntegral ? builder.Sub(leftOperand, rightOperand) : builder.FSub(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateMultiplication(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];
            var rightOperandType = operandTypes[1];

            if (leftOperandType != rightOperandType)
            {
                // case int * float, need to cast the int to float
                leftOperand = IntrinsicUnaryExpressions.GenerateIntrinsicCall(builder, leftOperand,
                    leftOperandType, rightOperandType.Name);
            }

            return rightOperandType.IsIntegral ? builder.Mul(leftOperand, rightOperand) : builder.FMul(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateDivision(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];
            var rightOperandType = operandTypes[1];

            if (leftOperandType != rightOperandType)
            {
                // case int / float, need to cast the int to float
                leftOperand = IntrinsicUnaryExpressions.GenerateIntrinsicCall(builder, leftOperand,
                    leftOperandType, rightOperandType.Name);
            }

            if (rightOperandType.IsIntegral)
            {
                if (((IntegerType) leftOperandType).IsSigned)
                {
                    return builder.SignedDiv(leftOperand, rightOperand);
                }

                return builder.UnsignedDiv(leftOperand, rightOperand);
            }

            return builder.FDiv(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateModulo(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            return ((IntegerType) leftOperandType).IsSigned ? builder.SignedRem(leftOperand, rightOperand) : builder.UnsignedRem(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateLessThan(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            if (leftOperandType.IsIntegral)
            {
                return ((IntegerType) leftOperandType).IsSigned
                    ? builder.SignedLT(leftOperand, rightOperand)
                    : builder.UnsignedLT(leftOperand, rightOperand);
            }

            return builder.FLT(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateLessOrEqual(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            if (leftOperandType.IsIntegral)
            {
                return ((IntegerType) leftOperandType).IsSigned
                    ? builder.SignedLE(leftOperand, rightOperand)
                    : builder.UnsignedLE(leftOperand, rightOperand);
            }

            return builder.FLE(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateGreaterThan(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            if (leftOperandType.IsIntegral)
            {
                return ((IntegerType) leftOperandType).IsSigned
                    ? builder.SignedGT(leftOperand, rightOperand)
                    : builder.UnsignedGT(leftOperand, rightOperand);
            }

            return builder.FGT(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateGreaterOrEqual(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            if (leftOperandType.IsIntegral)
            {
                return ((IntegerType) leftOperandType).IsSigned
                    ? builder.SignedGE(leftOperand, rightOperand)
                    : builder.UnsignedGE(leftOperand, rightOperand);
            }

            return builder.FGE(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateEqual(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            if (leftOperandType.IsIntegral || leftOperandType == CcashType.Boolean)
            {
                return builder.EQ(leftOperand, rightOperand);
            }

            return builder.FEQ(leftOperand, rightOperand);
        }

        private static LLVMValueRef GenerateNotEqual(IIRBuilder builder, LLVMValueRef[] operands, CcashType[] operandTypes)
        {
            var leftOperand = operands[0];
            var leftOperandType = operandTypes[0];
            var rightOperand = operands[1];

            if (leftOperandType.IsIntegral || leftOperandType == CcashType.Boolean)
            {
                return builder.NE(leftOperand, rightOperand);
            }

            return builder.FNE(leftOperand, rightOperand);
        }

        private static void AddOperators(CcashType type)
        {
            foreach (var method in type.Methods)
            {
                var operatorName = method.Name.Substring("operator".Length);
                Functions.Add(method.FullName, operatorName);
            }
        }

        private static void AddConstructors(CcashType type)
        {
            foreach (var constructor in type.Constructors)
            {
                var constructorFullName = FunctionNameMangler.Mangle(type.Name, constructor.ParameterTypes.ToArray());
                Functions.Add(constructorFullName, type.Name);
            }
        }
    }
}