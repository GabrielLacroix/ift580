using LLVMSharp;

namespace Ccash.CodeGeneration.LLVMGenerator
{
    public interface IIRBuilder
    {
        void PositionAtEnd(LLVMBasicBlockRef block);

        LLVMBasicBlockRef CurrentBlock { get; }

        void ReturnVoid();

        void Return(LLVMValueRef value);

        LLVMValueRef Add(LLVMValueRef left, LLVMValueRef right);

        LLVMValueRef FAdd(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef Sub(LLVMValueRef left, LLVMValueRef right);

        LLVMValueRef FSub(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef Mul(LLVMValueRef left, LLVMValueRef right);

        LLVMValueRef FMul(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef SignedDiv(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef UnsignedDiv(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FDiv(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef SignedRem(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef UnsignedRem(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef SignedLT(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef UnsignedLT(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FLT(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef SignedLE(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef UnsignedLE(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FLE(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef SignedGT(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef UnsignedGT(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FGT(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef SignedGE(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef UnsignedGE(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FGE(LLVMValueRef left, LLVMValueRef right);

        LLVMValueRef EQ(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef NE(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FEQ(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef FNE(LLVMValueRef left, LLVMValueRef right);

        LLVMValueRef Or(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef Xor(LLVMValueRef left, LLVMValueRef right);
        
        LLVMValueRef And(LLVMValueRef left, LLVMValueRef right);

        LLVMValueRef BitNot(LLVMValueRef value);
        
        LLVMValueRef Neg(LLVMValueRef value);
        
        LLVMValueRef FNeg(LLVMValueRef value);

        LLVMValueRef SignExtendOrCastInt(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef ZeroExtendOrCastInt(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef TruncateOrCastInt(LLVMValueRef value, LLVMTypeRef destinationType);

        LLVMValueRef CastInt(LLVMValueRef value, LLVMTypeRef destinationType);

        LLVMValueRef FloatToSigned(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef FloatToUnsigned(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef SignedToFloat(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef UnsignedToFloat(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef TruncateFloat(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef ExtendFloat(LLVMValueRef value, LLVMTypeRef destinationType);
        
        LLVMValueRef CallFunction(LLVMValueRef function, LLVMValueRef[] args);

        LLVMValueRef AllocateVariable(LLVMTypeRef type, string name);

        LLVMValueRef AddGlobalString(string name, string value);

        LLVMValueRef ReinterpretCast(LLVMValueRef value, LLVMTypeRef type);

        LLVMValueRef Ternary(LLVMValueRef condition, LLVMValueRef trueValue, LLVMValueRef falseValue);

        LLVMValueRef Load(LLVMValueRef variable);
        
        void Store(LLVMValueRef value, LLVMValueRef variable);

        void Branch(LLVMBasicBlockRef block);

        void ConditionalBranch(LLVMValueRef condition, LLVMBasicBlockRef trueBlock, LLVMBasicBlockRef falseBlock);
    }
}