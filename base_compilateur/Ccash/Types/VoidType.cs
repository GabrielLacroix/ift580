using System.Collections.Generic;

namespace Ccash.Types
{
    public class VoidType : CcashType
    {
        public override string Name => "void";

        public override List<FunctionType> Constructors => new List<FunctionType>();
        
        public override List<Method> Methods => new List<Method>();
    }
}