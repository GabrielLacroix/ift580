using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;

namespace Ccash.Types
{
    public class FunctionType : CcashType
    {
        public override string Name => $"func({string.Join(", ", ParameterTypes)}) {ReturnType?.ToString() ?? ""}";
        
        public override List<FunctionType> Constructors => new List<FunctionType>();
        
        public override List<Method> Methods => new List<Method>();

        public List<CcashType> ParameterTypes { get; }

        public CcashType ReturnType { get; }

        public FunctionType(CcashType returnType, params CcashType[] parameterTypes)
        {
            ParameterTypes = parameterTypes.ToList();
            ReturnType = returnType;
        }
        
        public FunctionType(CcashParser.FunctionDeclarationContext context)
        {
            ParameterTypes = context.functionParameters()?.variable()?.Select(v => Create(v.variableType())).ToList() ?? new List<CcashType>();
            ReturnType = context.returnType() == null ? CcashType.Void : Create(context.returnType());
        }

        public override bool Equals(CcashType other)
        {
            return base.Equals(other) 
                   && other is FunctionType otherFunc
                   && ParameterTypes.SequenceEqual(otherFunc.ParameterTypes)
                   && ReturnType == otherFunc.ReturnType;
        }
    }
}