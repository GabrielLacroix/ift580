using System;
using System.Collections.Generic;
using System.Linq;
using Ccash.Antlr;

namespace Ccash.Types
{
    public abstract class CcashType : IEquatable<CcashType>
    {
        public abstract string Name { get; }

        public virtual bool IsIntegral => false;

        public virtual bool IsFloatingPoint => false;

        public bool IsNumeric => IsIntegral || IsFloatingPoint;

        public abstract List<FunctionType> Constructors { get; }
        
        public abstract List<Method> Methods { get; }

        public static ReferenceType ConstRef(CcashType type)
        {
            return new ReferenceType(type, false);
        }

        public static ReferenceType MutRef(CcashType type)
        {
            return new ReferenceType(type, true);
        }

        public static CcashType Create(CcashParser.VariableTypeContext context)
        {
            if (context.valueType() != null)
            {
                return Create(context.valueType());
            }

            throw new NotImplementedException();
        }
        
        protected static CcashType Create(CcashParser.ReturnTypeContext context)
        {
            return Create(context.valueType());
        }

        private static CcashType Create(CcashParser.ValueTypeContext context)
        {
            switch (context)
            {
                case CcashParser.IntegralTypeContext integralTypeContext:
                {
                    return new IntegerType(integralTypeContext.GetText());
                }

                case CcashParser.FloatTypeContext floatTypeContext:
                {
                    return new FloatType(floatTypeContext.GetText());
                }

                case CcashParser.StringTypeContext _:
                {
                    return String;
                }

                case CcashParser.BoolTypeContext _:
                {
                    return Boolean;
                }

                default:
                    throw new NotImplementedException();
            }
        }

        public bool IsPromotable(CcashType destination) => PromotionDistance(destination) != int.MaxValue;

        public virtual int PromotionDistance(CcashType destination) => this == destination ? 0 : int.MaxValue;

        public override string ToString()
        {
            return Name;
        }

        public virtual bool Equals(CcashType other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (GetType() != other.GetType()) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CcashType);
        }

        public static bool operator ==(CcashType right, CcashType left)
        {
            return !ReferenceEquals(right, null) && (right.Equals(left));
        }

        public static bool operator !=(CcashType right, CcashType left)
        {
            return !(right == left);
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
        
        protected static IEnumerable<FunctionType> GenerateConstructorsFor<T>(T type) where T : CcashType
        {
            return AllPrimitives.Select(otherType => new FunctionType(type, otherType));
        }
        
        protected Method CreateConstOperator(string op, CcashType returnType, params CcashType[] parameters)
        {
            return new Method(this, $"operator{op}", false, returnType, parameters);
        }

        protected Method CreateMutOperator(string op, CcashType returnType, params CcashType[] parameters)
        {
            return new Method(this, $"operator{op}", true, returnType, parameters);
        }
        
        public static BooleanType Boolean => new BooleanType();
        public static IntegerType Int8 => new IntegerType(8, true);
        public static IntegerType Uint8 => new IntegerType(8, false);
        public static IntegerType Int16 => new IntegerType(16, true);
        public static IntegerType Uint16 => new IntegerType(16, false);
        public static IntegerType Int32 => new IntegerType(32, true);
        public static IntegerType Uint32 => new IntegerType(32, false);
        public static IntegerType Int64 => new IntegerType(64, true);
        public static IntegerType Uint64 => new IntegerType(64, false);
        public static FloatType Float32 => new FloatType(32);
        public static FloatType Float64 => new FloatType(64);
        public static ConstStringType String => new ConstStringType();
        public static VoidType Void => new VoidType();

        public static readonly IntegerType[] IntegerTypes =
        {
            Int8, Uint8, Int16, Uint16, Int32, Uint32, Int64, Uint64
        };

        public static readonly FloatType[] FloatTypes = {Float32, Float64};

        public static readonly CcashType[] AllPrimitives =
            new[] {Boolean}.Concat(IntegerTypes.Concat<CcashType>(FloatTypes)).ToArray();
    }
}