using System.Collections.Generic;

namespace Ccash.Types
{
    public class ConstStringType : CcashType
    {
        public override string Name => "string";

        public override List<FunctionType> Constructors => new List<FunctionType>();
        
        public override List<Method> Methods => new List<Method>();
    }
}