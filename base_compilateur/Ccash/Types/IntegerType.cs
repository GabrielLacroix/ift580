using System.Collections.Generic;
using System.Linq;

namespace Ccash.Types
{
    public class IntegerType : CcashType
    {
        public override string Name => $"{(IsSigned ? "" : "u")}int{Size}";

        public override bool IsIntegral => true;

        public override List<FunctionType> Constructors => GenerateConstructorsFor(this).ToList();

        public override List<Method> Methods => GenerateMethods();

        public uint Size { get; }

        public bool IsSigned { get; }

        public IntegerType(string name)
        {
            IsSigned = !name.StartsWith('u');
            Size = uint.TryParse(name.Substring(name.Length - 2), out var size) ? size : 8;
        }

        public IntegerType(uint size, bool isSigned)
        {
            Size = size;
            IsSigned = isSigned;
        }

        public override bool Equals(CcashType other)
        {
            return base.Equals(other)
                   && other is IntegerType otherInt
                   && Size == otherInt.Size
                   && IsSigned == otherInt.IsSigned;
        }

        public override int PromotionDistance(CcashType destination)
        {
            if (destination is IntegerType)
                return 0;
            if (destination is FloatType)
                return 1;
            return int.MaxValue;
        }

        private List<Method> GenerateMethods()
        {
            var methods = new[] {"<", "<=", ">", ">=", "==", "!="}.Select(op => CreateConstOperator(op, Boolean, this))
                .Concat(new[] {"+", "-", "*", "/", "^", "&", "|", "%"}.Select(op => CreateConstOperator(op, this, this)))
                .Concat(new[] {"+", "-", "*", "/",}.Select(op => CreateConstOperator(op, Float32, Float32)))
                .Concat(new[] {"+", "-", "*", "/",}.Select(op => CreateConstOperator(op, Float64, Float64)))
                .Concat(new[] {":=", "+=", "-=", "/=", "*=", "&=", "|=", "^=", "%=" }.Select(op => CreateMutOperator(op, Void, this)))
                .Concat(new[] {CreateConstOperator("!", this)});

            return IsSigned ? methods.Concat(new[] {CreateConstOperator("-", this)}).ToList() : methods.ToList();
        }
    }
}