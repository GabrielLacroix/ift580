﻿using System.Collections.Generic;
using System.Linq;

namespace Ccash.Types
{
    public class FloatType : CcashType
    {
        public override string Name => $"float{Size}";
        
        public override List<FunctionType> Constructors => GenerateConstructorsFor(this).ToList();

        public override List<Method> Methods => GenerateMethods();

        public override bool IsFloatingPoint => true;

        public uint Size { get; }

        public FloatType(string name)
        {
            Size = uint.TryParse(name.Substring(name.Length - 2), out var size) ? size : 32;
        }

        public FloatType(uint size)
        {
            Size = size;
        }
        
        public override bool Equals(CcashType other)
        {
            return base.Equals(other)
                   && other is FloatType otherFloat
                   && Size == otherFloat.Size;
        }

        public override int PromotionDistance(CcashType destination)
        {
            if (destination is FloatType)
                return 0;
            if (destination is IntegerType)
                return 1;
            return int.MaxValue;
        }

        private List<Method> GenerateMethods()
        {
            return new[] {"<", "<=", ">", ">=", "==", "!="}.Select(op => CreateConstOperator(op, Boolean, this))
                .Concat(new[] {"+", "-", "*", "/"}.Select(op => CreateConstOperator(op, this, this)))
                .Concat(new[] {":=", "+=", "-=", "/=", "*="}.Select(op => CreateMutOperator(op, Void, this)))
                .Concat(new[] {CreateConstOperator("-", this)})
                .ToList();
        }
    }
}