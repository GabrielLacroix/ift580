using System.Collections.Generic;
using System.Linq;

namespace Ccash.Types
{
    public class BooleanType : CcashType
    {
        public override string Name => "bool";

        public override List<FunctionType> Constructors => GenerateConstructorsFor(this).ToList();

        public override List<Method> Methods => GenerateMethods();

        private List<Method> GenerateMethods()
        {
            return new[] {"&&", "||", "^", "==", "!=", "&=", "|=", "^="}.Select(op => CreateConstOperator(op, Boolean, this))
                .Concat(new[] {CreateConstOperator("!", this)})
                .Concat(new[] {CreateMutOperator(":=", Void, this)})
                .ToList();
        }
    }
}