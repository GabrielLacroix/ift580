using System.Linq;

namespace Ccash.Types
{
    public class Method
    {
        public CcashType OwnerType { get; }

        public string Name { get; }

        public string FullName => FunctionNameMangler.Mangle($"{OwnerType.Name}::{Name}", FunctionType.ParameterTypes.ToArray());

        public FunctionType FunctionType { get; }
        
        public bool IsMutable { get; }

        public Method(CcashType ownerType, string name, bool isMutable, CcashType returnType, params CcashType[] parameters)
        {
            OwnerType = ownerType;
            Name = name;
            IsMutable = isMutable;
            var selfParameter = IsMutable ? CcashType.MutRef(OwnerType) : OwnerType;
            FunctionType = new FunctionType(returnType, new[] {selfParameter}.Concat(parameters).ToArray());
        }
    }
}