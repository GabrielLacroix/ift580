﻿using System.Collections.Generic;

namespace Ccash.Types
{
    public class ReferenceType : CcashType
    {
        public CcashType Type { get; }
        
        public bool IsMutable { get; }
        
        public override string Name => $"{(IsMutable ? "mut" : "const")} &{Type.Name}";
        
        public override List<FunctionType> Constructors => new List<FunctionType>();
        
        public override List<Method> Methods => new List<Method>();
        
        public ReferenceType(CcashType type, bool isMutable)
        {
            Type = type;
            IsMutable = isMutable;
        }
        
        public override bool Equals(CcashType other)
        {
            return base.Equals(other) 
                   && other is ReferenceType otherRef 
                   && Type == otherRef.Type 
                   && IsMutable == otherRef.IsMutable;
        }
    }
}