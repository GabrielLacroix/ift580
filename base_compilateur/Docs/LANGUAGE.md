# C$ language documentation

C$ was designed at first to be something of a cross between the features of C#' and the semantics of C++. Currently, C$ is still in the very early stages, and so mostly only supports a subset of the functionalities of C.


## Program Structure

A C$ program, much like in most languages, starts with a main function.  

```go
/* Block comments! */

// My first C$ program
func main() int32 {

    printf("Hello World!");
    
    return 0;
}
```
As we can see, code commends function similarly to the ones in C++ and C#.


## Writing on stdout

C$ has a `printf` function that works more or less identically to the one found in C.


## Variable Declarations

We wished for C$ to be very explicit in the mutability of objects, and every variable declaration begins with a mutability specifier, `mut` or `const`. In addition, we have opted to use `:=` as the assignment operator. Putting those two together, a variable declaration will generally look like this:
```rust
mut int32 a := 5;
```

The language currently supports only a few primitives types: `int8`, `uint8`, `int16`, `uint16`, `int32`, `uint32`, `int64`, `uint64`, `float32`, `float64`, `bool`, and `string`.


## Function Declarations

The main particularities of C$ function definitions are that they begin with the `func` keyword and end with the return type of the function. In addition, we require that function parameters have a mutability specifier, even if they are passed by value. While that may seem strange, it allows us to do some optimizations in the future, such as only doing a shallow copy of an object if it is passed as a `const` value.
```go
func max(const int32 nb1, const int32 nb2) int32 {
    if nb1 > nb2 {
        return nb1;
    }
    return nb2;
}

func max(const float64 nb1, const float64 nb2) float64 {
    if nb1 > nb2 {
        return nb1;
    }
    return nb2;
}
```

As one may notice, function overloading is supported by the language.


## If Statements

The only difference with the `if` in C, C++, or C# is that parentheses are not required for the condition expression. This is true of the loop statements as well.
```c++
if a < 5 {
    // ...
}
else if a < 10 {
    // ...
}
else{
    // ...
}
```

## While Loops

```cpp
while true {
    /* Code here */
}
```

## For Loops
```rust
for mut int32 i := 0; i < 10; i += 1 {
    /* Code here */
}
```


## Operators

These are the base operators currently supported by the language, listed with decreasing priority:

|Operator                       |Name                     |
|-------------------------------|:------------------------|
|- !                            |Unary minus and not      |
|* / %                          |Multiply, Divide, Modulo |
|+ -                            |Addition, Substraction   |
|< > <= >=                      |Comparaison              |
|== !=                          |Equality                 |
|&                              |Bitwise And              |
|^                              |Bitwise Xor              |
|\|                             |Bitwise Or               |
|&&                             |And                      |
|\|\|                           |Or                       |
|:=                             |Assignment               |
|*= /= %= += <br> -= &= ^= \|=  |Compound assignment      |

Of note is that the unary `!` operator pulls double duty as the bitwise Not operator when used on integer types instead of the more traditionnal `~`.
