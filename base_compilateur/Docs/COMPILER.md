# Ccash compiler documentation

## Architecture overview

The compiler is split into two projects, `Ccash.Antlr` and `Ccash`. `Ccash.Antlr` contains the grammar file for C$, `Ccash.g4`. Whenever the project is built, Antlr is used to generate the lexer and parser from the grammar file. It is linked as a library by `Ccash`, which should always force a rebuild of `Ccash.Antlr`, ensuring that the generated code is always up to date.

The entry point of the compiler is located in the `Ccash.cs` file, which calls the rest of the code to handle parsing, semantic analysis, and code generation.


### Lexical and Syntactic analysis

This is entirely handled by Antlr, which takes care of generating a basic Abstract Syntax Tree for us.


### Semantic analysis

The AST we obtain through Antlr is passed to the constructor of our own AbstractSyntaxTree structure, which will walk the tree and perform semantic analysis on the nodes. 

Since every function needs to have a unique symbol, both for our Symbol Table and for the LLVM IR, function overloading is realized by mangling the name of the function with the type information of its parameters. This is handled by the `FunctionNameMangler` class. It is mainly used when adding the functions and methods of primitive types to the top-level Symbol Table, and by the `FunctionOverloadResolver`. As its name suggests, the `FunctionOverloadResolver` figures out which of a function's overloads to call given a set of arguments. This is where implicit type conversions may be inserted, such as `int16` -> `int32`.


### Code generation

The generation of LLVM IR works, much like the semantic analysis, by walking the AST and generating the appropriate IR. This is facilitated by the `IRBuilder` class, which acts as a convenient wrapper around LLVMSharp's builder. 

The classes in the Intrinsics folder handle the generation of code that LLVM innately understands, such as most operations on primitive types. The rest of the heavy lifting is done by `StatementGenerator` and `ExpressionGenerator`.

## Output

The compiler will output two files for a given `.ccash` file: a `.bc` file and `.ir` file. The `.bc` file is LLVM bitcode that can be executed by `lli` or compiled into an executable with `clang`, while the `.ir` file simply contains the generated LLVM IR in readable text and can be viewed with any text editor.

When running the compiler through Visual Studio, the executable and generated files will be in `Ccash/bin/Debug/netcoreapp2.2/netcoreapp3.0`.

