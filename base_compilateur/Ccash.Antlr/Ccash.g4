grammar Ccash;

compilationUnit : (functionDeclaration | classDeclaration)* EOF;

functionDeclaration : 'func' Identifier '(' functionParameters? ')' returnType? block;

functionParameters : variable (',' variable)*;
    
functionCall : functionName '(' functionArgs? ')';

functionArgs : expression (',' expression)*;

classDeclaration : 'class' Identifier '{' '}';

variableDeclaration : variable ':=' expression;

variable : ('const' | 'mut') variableType Identifier;

 functionName
    : Identifier
    | Bool
    | Int8
    | Uint8
    | Int16
    | Uint16
    | Int32
    | Uint32
    | Int64
    | Uint64
    | Float32
    | Float64;

/***************************************************
 *                                                 *
 *                      Types                      *
 *                                                 *
 **************************************************/

valueType 
    : 'bool'                            # BoolType
    | 'string'                          # StringType
    | 'int8'                            # IntegralType
    | 'uint8'                           # IntegralType
    | 'int16'                           # IntegralType
    | 'uint16'                          # IntegralType
    | 'int32'                           # IntegralType
    | 'uint32'                          # IntegralType
    | 'int64'                           # IntegralType
    | 'uint64'                          # IntegralType
    | 'float32'                         # FloatType
    | 'float64'                         # FloatType
    | Identifier                        # ClassType
    | '*' ('const' | 'mut') valueType   # PointerType
    ;
    
referenceType : '&' valueType;

arrayType : '[]' (valueType | referenceType | arrayType);

variableType : valueType | referenceType | arrayType;

returnType : valueType | (('const' | 'mut') referenceType);

/***************************************************
 *                                                 *
 *                   Statements                    *
 *                                                 *
 **************************************************/

statement 
    : block                                     #BlockStatement
    | ifStatement                               #ConditionalStatement
    | switchStatement                           #ConditionalStatement
    | functionCall ';'                          #FunctionCallStatement
    | variableDeclaration ';'                   #VariableDeclarationStatement
    | reassignment ';'                          #ReassignmentStatement
    | whileStatement                            #LoopStatement
    | forStatement                              #LoopStatement
    | doStatement                               #LoopStatement
    | repeatStatement                           #LoopStatement
    | breakStatement                            #ControlFlowStatement
    | continueStatement                         #ControlFlowStatement
    | returnStatement                           #ControlFlowStatement;

block : '{' statement* '}';

ifStatement : 'if' expression block elseIfStatement* elseStatement?;

elseIfStatement : 'else' 'if' expression block;

elseStatement : 'else' block;

switchStatement : fallthrough? 'switch' expression '{' caseStatement+ defaultStatement? '}';

caseStatement : 'case' expression caseBlock;

caseBlock : '{' statement* fallthrough?'}';

fallthrough : 'fallthrough' ';'?;

defaultStatement: 'default' block;

returnStatement : 'return' expression? ';';

reassignment 
    : Identifier ':=:' Identifier
    | Identifier ':=' expression
    | Identifier ('+='|'-='|'/='|'*=') expression
    | Identifier ('%='|'&='|'^='|'|=') expression;

whileStatement : 'while' expression block;

forStatement : 'for' forInitialization? ';' expression? ';' forUpdate? block; 

doStatement : 'do' block 'while' expression ';';

repeatStatement : 'repeat' expression block;

forInitialization
    : functionCall
    | variableDeclaration
    | reassignment;

forUpdate
    : functionCall
    | reassignment;

breakStatement : 'break' ';';

continueStatement : 'continue';

/***************************************************
 *                                                 *
 *                   Expressions                   *
 *                                                 *
 **************************************************/

expression
    : '(' expression ')'                               # ParenthesisExpression
    | IntegerLiteral                                   # IntegerLiteralExpression
    | FloatLiteral                                     # FloatLiteralExpression
    | Float32Literal                                   # Float32LiteralExpression
    | StringLiteral                                    # StringLiteralExpression
    | Identifier                                       # IdentifierExpression
    | ('true' | 'false')                               # BooleanLiteralExpression
    | functionCall                                     # FunctionCallExpression
    | ('-' | '!' | '*' | '&') expression               # UnaryOperatorExpression
    | expression ('*' | '/' | '%') expression          # BinaryOperatorExpression
    | expression ('+' | '-') expression                # BinaryOperatorExpression
    | expression ('<<' | '>>') expression              # BinaryOperatorExpression
    | expression ('<' | '<=' | '>' | '>=') expression  # BinaryOperatorExpression
    | expression ('==' | '!=') expression              # BinaryOperatorExpression
    | expression '&' expression                        # BinaryOperatorExpression
    | expression '^' expression                        # BinaryOperatorExpression
    | expression '|' expression                        # BinaryOperatorExpression
    | expression '&&' expression                       # BinaryOperatorExpression
    | expression '||' expression                       # BinaryOperatorExpression
    | expression '?' expression ':' expression         # TernaryExpression;

/***************************************************
 *                                                 *
 *                      Lexer                      *
 *                                                 *
 **************************************************/

Int8    : 'int8';
Uint8   : 'uint8';
Int16   : 'int16';
Uint16  : 'uint16';
Int32   : 'int32';
Uint32  : 'uint32';
Int64   : 'int64';
Uint64  : 'uint64';

Float32 : 'float32';
Float64 : 'float64';

Bool    : 'bool';
True    : 'true';
False   : 'false';
String  : 'string';

LeftParen		: '(';
RightParen		: ')';
LeftBracket		: '[';
RightBracket	: ']';
LeftBrace		: '{';
RightBrace		: '}';
DoubleQuote     : '"';
SingleQuote     : '\'';

Return  : 'return';
Func    : 'func';
Const   : 'const';
Mut     : 'mut';
If      : 'if';
Else    : 'else';
While   : 'while';
For     : 'for';

Plus        : '+';
Minus       : '-';
Mul         : '*';
Div         : '/';
Mod         : '%';
BitAnd      : '&';
BitOr       : '|';
BitXor      : '^';
Not         : '!';
LogicalAnd  : '&&';
LogicalOr   : '||';
Lesser      : '<';
LesserEq    : '<=';
Greater     : '>';
GreaterEq   : '>=';
Equal       : '==';
NotEqual    : '!=';
Ternary     : '?';
Swap        : ':=:';
Assign      : ':=';
AddAssign   : '+=';
SubAssign   : '-=';
DivAssign   : '/=';
MulAssign   : '*=';
ModAssign   : '%=';
AndAssign   : '&=';
OrAssign    : '|=';
XorAssign   : '^=';

Semicolon : ';';

Identifier : Letter (Letter | Digit)*;

fragment Letter : [a-zA-Z_];

fragment Digit : [0-9];

IntegerLiteral : '-'?Digit+;

FloatLiteral : '-'?Digit+'.'Digit+;

Float32Literal : FloatLiteral'f';

StringLiteral : '"' .*? '"';

Whitespace : [ \t]+ -> skip;

Newline : ('\r' '\n'? | '\n') -> skip;

BlockComment : '/*' .*? '*/' -> skip;

LineComment : '//' ~[\r\n]* -> skip;
