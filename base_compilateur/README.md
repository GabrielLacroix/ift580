# IFT580 - Compilateur

Assignement for IFT580 - Compiling and languages interpretation

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* `java` must be installed and present in path (we must be able to use the `java` command in a terminal).

* `.NET Core 3.0` must be installed. On Windows, Visual Studio 2019 should take care of it. On Ubuntu 18.04, use the following commands:
  ```bash
  wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
  sudo dpkg -i packages-microsoft-prod.deb
  sudo add-apt-repository universe
  sudo apt-get update
  sudo apt-get install apt-transport-https
  sudo apt-get update
  sudo apt-get install dotnet-sdk-3.0
  ```

  Make sure that the `dotnet` command works. Other versions of `.NET Core` may work, but haven't been tested.

* `Clang` must be installed, which should come with all the `LLVM` tools. They should all be present in path. On Windows, installing Cygwin is the easiest way to get them.

### Installation / Building

Simply build the solution in Visual Studio 2019, or use the following command :

```
dotnet build Ccash.sln
```

## Compiling one of the C$ test files

```bash
cd Ccash
dotnet run test_files/main.ccash
```

## Useful commands

Executing the `LLVM` bytecode produced by the compiler:

```bash
lli main.bc
```

This may fail on Windows, simply use `clang` instead:
```bash
clang main.bc
a.exe
```

Viewing the `LLVM IR` produced by the compiler:

```bash
llvm-dis -o - main.bc | less
```

or

```bash
cat main.ir | less
```
