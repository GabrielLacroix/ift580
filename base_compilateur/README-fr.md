# IFT580 - Compilateur

Implémentation du devoir pour IFT580 - Compilation et interprétation des langages

## Premier pas

Ces instructions vous fourniront une copie du projet opérationnel sur votre ordinateur local à des fins de développement et de test.

### Prérequis

* `java` doit être installé et accessible via les variables d'environnement (vous devez pouvoir utiliser la commande` java` dans un terminal).

* `.NET Core 3.0` doit être installé. Sous Windows, Visual Studio 2019 devrait s'en charger. Sur Ubuntu 18.04, utilisez les commandes suivantes:
  ```bash
  wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
  sudo dpkg -i packages-microsoft-prod.deb
  sudo add-apt-repository universe
  sudo apt-get update
  sudo apt-get install apt-transport-https
  sudo apt-get update
  sudo apt-get install dotnet-sdk-3.0
  ```

  Assurez-vous que la commande `dotnet` fonctionne. D'autres versions de `.NET Core` peuvent fonctionner, mais n'ont pas été testées.

* `Clang` doit être installé avec tous les outils `LLVM`. Ils devraient tous être accessible via les variables d'environnement. Sur Windows, Cygwin est la façon la plus simple de les installer.

### Installation / Compilation

Générez simplement la solution dans Visual Studio 2019 ou utilisez la commande suivante :

```
dotnet build Ccash.sln
```

## Compilation d'un des fichiers tests en C$

```bash
cd Ccash
dotnet run test_files/main.ccash
```

## Commandes utiles

Exécuter le bytecode `LLVM` produit par le compilateur:

```bash
lli main.bc
```

Cela pourrait ne pas fonctionner sur Windows. Dans ce cas, simplement faire:
```bash
clang main.bc
a.exe
```

Visualisation du `LLVM IR` produit par le compilateur:

```
llvm-dis -o - main.bc | less
```

ou

```bash
cat main.ir | less
```
